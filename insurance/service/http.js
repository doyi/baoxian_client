
var rootDocment = 'https://xiaochengxu.rongyip.com';
var header = {
  'Accept': 'application/json',
  'content-type': 'application/x-www-form-urlencoded',
  //'Authorization': {'content-type': 'application/x-www-form-urlencoded'},
  // 'Authorization': { 'content-type': 'application/json', 'Cookie': 'JSESSIONID=' + sessionId },
}
function getReq(url ,data, cb, isShow) {
  header.token = wx.getStorageSync('token') || ''
  url = url +'?'
  if(data){
    for(var params in data){
      url +=  params + '=' + data[params] + '&'
    }
  }else{
    data = cb
  }
  if(!isShow){
    wx.showLoading({
      title: '加载中',
    })
  }
 
  wx.request({
    url: rootDocment + url,
    method: 'get',
    header: header,
    success: function (res) {
      wx.hideLoading();
      // return typeof cb == "function" && cb(res.data)
      if (res.data.code == 200) {
        return typeof cb == "function" && cb(res.data)
      } else if (res.data.code == 4604) {
        if(getApp().globalData.time){
          clearInterval(getApp().globalData.time);
        }else{
          wx.showModal({
            showCancel: false,
            title: '错误',
            content: res.data.msg,
            success: function (res) {
              if (res.confirm) {
                wx.reLaunch({
                  url: '/pages/login/login/login',
                });
              }
            }
          })
        }
      } else {
        wx.showModal({
          showCancel:false,
          title: '错误',
          content: res.data.msg,
        })
      }
    },
    fail: function () {
      wx.hideLoading();
      wx.showModal({
        title: '网络错误',
        content: '网络出错，请刷新重试',
        showCancel: false
      })
      return typeof cb == "function" && cb(false)
    }
  })
}

function postReq(url, data, cb) {
  header.token = wx.getStorageSync('token') || ''

  // data.token = wx.getStorageSync('token')||'';
  wx.showLoading({
    title: '加载中',
  })
    wx.request({
      url: rootDocment + url,
      header: header,
      data: data,
      method: 'post',
      success: function (res) {
        wx.hideLoading();
        // return typeof cb == "function" && cb(res.data)
        if (res.data.code == 200) {
          return typeof cb == "function" && cb(res.data)
        } else if (res.data.code == 4604) {
          clearInterval(getApp().globalData.time);
          wx.showModal({
            showCancel: false,
            title: '错误',
            content: res.data.msg,
            success: function (res) {
              if (res.confirm) {
                wx.reLaunch({
                  url: '/pages/login/login/login',
                });
              }
            }
          })
        } else {
          wx.showModal({
            showCancel: false,
            title: '错误',
            content: res.data.msg,
          })
        }
      },
      fail: function () {
        wx.hideLoading();
        wx.showModal({
          title: '网络错误',
          content: '网络出错，请刷新重试',
          showCancel: false
        })
        return typeof cb == "function" && cb(false)
      }
    })

}
module.exports = {
  getReq: getReq,
  postReq: postReq,
  header: header,
}
