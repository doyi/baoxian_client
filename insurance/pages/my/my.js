// pages/my/my.js
const app = getApp()
var http = require('../../service/http.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    username: '',
    head_img: '',
    card_num: '',
    company_name: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(app)
    app.tabBar();

    http.postReq('/index/user/getUserInfo', {}, res => {
      console.log(res)
      this.setData({
        role_id: wx.getStorageSync('role_id'),
        username: res.data.username,
        head_img: res.data.head_img,
        card_num: res.data.card_num,
        company_name: res.data.company_name
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  skippage:function (e){
    var link = e.currentTarget.dataset.url;
    wx.navigateTo({url: link})
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // this.setData({
    //   username: app.globalData.userInfo.username,
    //   img: app.globalData.userInfo.img
    // });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
