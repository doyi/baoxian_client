// pages/my/myInfo/myInfo.js
const app = getApp();
var http = require('../../../service/http.js');
var wechat = require('../../../utils/wechat.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    role: app.globalData.userInfo.name,
    username: '',
    head_img: '',
    mobile: '',
    card_num: '',

    // bxr
    company_name:'',
    // dlr
    rem_link_name: '',
    rem_link_phone: '',

    sale_man_company: '',
    sale_man_linkman: '',

  },
  on_sale_man_linkman(e){

    this.setData({ sale_man_linkman: e.value.detail });
  },
  editAvatar() {

    wechat.chooseImage()
      .then(d => {
        let { path, size } = d.tempFiles[0];
        // let params = this.data.present_img;
        // params.push(path)
        // this.setData({
        //   tempImagePath: path,
        // });
        // console.log(path)
        return wechat.uploadFile("/index/Upload/index", path, "avatar")
      })
      .then(d => {
        console.log(d)
        let res = JSON.parse(d.data)
        let head_img = res.data.url
        console.log(head_img)
        this.setData({
          head_img: head_img
        })

      })
      .catch(e => {
        console.log(e);
      })
  },
  onCard_num(e){
    this.setData({ card_num: e.detail.value });
  },
  onInput(e) {
    this.setData({ username: e.detail.value});
  },
  onComInput(e) {
    this.setData({ company_name: e.detail.value});
  },
  onSubmit() {
    var that = this;
    http.postReq('/index/user/edit_info',{
      username: this.data.username,
      head_img: this.data.head_img,
      card_num: this.data.card_num,
      company_name: this.data.company_name
    }, res => {this.data.username
      // app.globalData.userInfo.username = this.data.username
      // app.globalData.userInfo.img = this.data.head_img
      wx.showToast({
        title: res.msg,
        icon:'success',
        duration:2000
      });
      setTimeout(function(){
        wx.navigateTo({
          url: '../my',
        })
      },2000)
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var role = '';
    switch (wx.getStorageSync('role_id')) {
      case '1':
        role = 'bxr';
        break;
      case '2':
        role = 'dlr';
        break;
      case '3':
        role = 'bbxr';
        break;
      case '4':
        role = 'ywy';
        break;
    }
    http.postReq('/index/user/getUserInfo', {},res=>{
      this.setData({
        role: role,
        username: res.data.username ||'',
        card_num: res.data.card_num ||'',
        head_img: res.data.head_img || '',
        mobile: res.data.mobile || '',
        company_name: res.data.company_name || '',
        rem_link_name: res.data.rem_link_name || '',
        rem_link_phone: res.data.rem_link_phone || '',
        sale_man_company: res.data.sale_man_company || '',
        sale_man_linkman: res.data.sale_man_linkman || '',
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
