// pages/login/register/register.js
const app = getApp()
var http = require('../../../service/http.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    code_id: '',
    isShow: false,
    phone:'',
    code:'',
    codeShow:false,
    codeTime: 30,
    next_btn:false,
    checkbox:'1',
  },
  showBtn() {
    if(this.data.phone.length ==11 && this.data.code !=''){
      this.setData({
        next_btn:true
      })
    } else {
      this.setData({
        next_btn: false
      })
    }
  },
  getPhone(e) {
    this.data.isShow = e.detail.value ? true : false
    this.setData({
      isShow: this.data.isShow,
      phone: e.detail.value
    })
    this.showBtn()
  },
  getCode(e) {
    this.setData({
      code: e.detail.value
    })
    this.showBtn()
  },
  postCode() {
    if(this.data.phone.length!=11){
      wx.showModal({
        title: '提示',
        content: '手机号不正确',
        showCancel:false,
        success(res) {
          return false
        }
      })
    }else{
      http.postReq('/index/Login/sendSms', {
      phone: this.data.phone,
      type: 2
    }, res => {
      this.setData({
        code_id: res.data.code_id
      })
      this.showTime()
    })
    }
  },
  delPhone(e){
    this.setData({
      isShow: false,
      phone: ''
    })
    this.showBtn()
  },
  showTime() {
    this.setData({
      codeShow:true
    })
    var time = setInterval(() => {
      var _time = this.data.codeTime
      if (_time == 0){
        this.setData({
          codeShow: false,
          codeTime:30
        })
        clearInterval(time)
      }else{
        this.setData({
          codeTime: _time -1
        })
      }
    }, 1000)
  },
  checkboxChange(e){
    console.log(e);
    this.setData({
      checkbox: !this.state.checkbox
    })
  },
  next_tap(){
    if (this.data.checkbox){
      http.postReq('/index/Login/register', {
        phone: this.data.phone,
        code: this.data.code,
        tickets: this.data.code_id
      }, res => {
        app.globalData.userInfo = res.data;
        if (res.data.role_id == '1') {
          app.globalData.userInfo.name = 'bxr';
          wx.navigateTo({
            url: `../checkInfo/bxrInfo/bxrInfo?uid=${res.data.uid}&userinfo=${encodeURIComponent(JSON.stringify(data))}`,
          })
        } if (res.data.role_id == '2') {
          app.globalData.userInfo.name = 'dlr';
          wx.navigateTo({
            url: `../checkInfo/dlrInfo/dlrInfo?uid=${res.data.uid}&userinfo=${encodeURIComponent(JSON.stringify(data))}`,
          })
        } if (res.data.role_id == '3') {
          app.globalData.userInfo.name = 'bbxr';
          wx.navigateTo({
            url: `../checkInfo/bbxrInfo/bbxrInfo?uid=${res.data.uid}&userinfo=${encodeURIComponent(JSON.stringify(data))}`,
          })
        } if (res.data.role_id == '4') {
          app.globalData.userInfo.name = 'ywy';
          wx.navigateTo({
            url: `../checkInfo/ywyInfo/ywyInfo?uid=${res.data.uid}&userinfo=${encodeURIComponent(JSON.stringify(data))}`,
          })
        }
        console.log(res)
      })
    }else{
      wx.showModal({
        title: '提示',
        content: '请同意服务条款',
        showCancel: false,
        success(res) {
          return false
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  login: function (options) {
    wx.reLaunch({
      url: '../login/login'
    })
  },
})
