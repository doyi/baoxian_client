// pages/login/login/login.js
var http = require('../../../service/http.js');
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShow: false,
    phone: '',
    code: '',
    codeShow: false,
    codeTime: 30,
    next_btn: false,
    checkbox: '1',
    tickets: ''
  },
  // 登录
  next_tap: function (options) {

    console.log(this.data.checkbox)
    http.postReq('/index/Login/login', {
      phone: this.data.phone,
      code: this.data.code,
      tickets: this.data.tickets
    }, res => {
      console.log(res)
    wx.setStorageSync('token', res.data.token);
    wx.setStorageSync('role_id', res.data.role_id);
    wx.setStorageSync('userInfo', res.data);
    app.globalData.userInfo = res.data;
    if (res.data.role_id == '1') {
      app.globalData.userInfo.name = 'bxr'
    } else if (res.data.role_id == '3') {
      app.globalData.userInfo.name = 'bbxr'
    } else if (res.data.role_id == '2') {
      app.globalData.userInfo.name = 'dlr'
    } else if (res.data.role_id == '4') {
      app.globalData.userInfo.name = 'ywy'

    } else {
      wx.showModal({
        title: '提示',
        content: '',
        showCancel: false,
        success(res) {
          return false
        }
      })
    }

    wx.reLaunch({
      url: '../../index/index'
    })
  })
    // wx.navigateTo({
    //   url: '../../index/index'
    // })
  },
  showBtn() {
    if (this.data.phone.length == 11 && this.data.code != '') {
      this.setData({
        next_btn: true
      })
    } else {
      this.setData({
        next_btn: false
      })
    }
  },
  getPhone(e) {
    this.data.isShow = e.detail.value ? true : false
    this.setData({
      isShow: this.data.isShow,
      phone: e.detail.value
    })
    this.showBtn()
  },
  getCode(e) {
    this.setData({
      code: e.detail.value
    })
    this.showBtn()
  },
  postCode() {
    if (this.data.phone.length != 11) {
      wx.showModal({
        title: '提示',
        content: '手机号不正确',
        showCancel: false,
        success(res) {
          return false
        }
      })
    } else {
      http.postReq('/index/Login/sendSms', {
        phone: this.data.phone,
        type: 3
      }, res => {
        this.setData({
        tickets: res.data.code_id
      })
      console.log(res)
      this.showTime()
    })
    }
  },
  delPhone(e) {
    this.setData({
      isShow: false,
      phone: ''
    })
    this.showBtn()
  },
  showTime() {
    this.setData({
      codeShow: true
    })
    var time = setInterval(() => {
      var _time = this.data.codeTime
      if (_time == 0) {
        this.setData({
          codeShow: false,
          codeTime: 30
        })
        clearInterval(time)
      } else {
        this.setData({
          codeTime: _time - 1
        })
      }
    }, 1000)
  },
  // 忘记密码
  forgotPwd: function (options) {
    wx.navigateTo({
      url: '../../my/revisePwd/revisePwd'
    })
  },
  // 注册
  register: function (options) {
    wx.navigateTo({
      url: '../register/register'
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('login: options', options);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  login: function (options) {
    wx.reLaunch({
      url: '../login/login'
    });
  },
})
