// pages/login/start.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    setTimeout(() => {
      var token = wx.getStorageSync('token');
      var role_id = wx.getStorageSync('role_id');
      app.globalData.userInfo = wx.getStorageSync('userInfo');
      if (token && role_id && app.globalData.userInfo) {
        switch (role_id) {
          case '1':
            app.globalData.userInfo.name = 'bxr';
            break;
          case '2':
            app.globalData.userInfo.name = 'dlr';
            break;
          case '3':
            app.globalData.userInfo.name = 'bbxr';
            break;
          case '4':
            app.globalData.userInfo.name = 'ywy'
            break;
          default:
            break;
        }
        var showLogin = true;
        if (showLogin) {
          wx.reLaunch({
            url: '../index/index'
          });
        } else {
          wx.reLaunch({
            url: '../public/webView/webView'
          });
        }
      } else {
        wx.reLaunch({
          url: './login/login'
        });
      }
    }, 2000);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
});
