//index.js

//获取应用实例
const app = getApp()
var http = require('../../service/http.js');
Page({
  data: {
    username: '',
    img: '',
    company_name: '',
    character: '',
    userInfo: [],
    userList: {
      bxr: [
        { url: '/pages/index/bxr/bdataEntry/bdataEntry', imgUrl: '../../icons/bao.png', name: '保险录入' },
        { url: '/pages/index/bxr/entryList/entryList', imgUrl: '../../icons/mingdan.png', name: '代录人名单' },
        { url: '/pages/index/bxr/bxrHisData/bxrHisData', imgUrl: '../../icons/lishi.png', name: '历史资料' },
        { url: '/pages/index/newHelp/newHelp', imgUrl: '../../icons/xinshou.png', name: '新手教程' },
        { url: '/pages/index/stampDraft/stampDraft', imgUrl: '../../icons/shouquan.png', name: '打印授权书' },
      ],
      bbxr: [
        // { url: '/pages/index/bbxr/checkData/checkData', imgUrl: '../../icons/hedui.png', name: '核对资料' },
        { url: '/pages/index/bbxr/bbxrHisData/bbxrHisData', imgUrl: '../../icons/lishi.png', name: '历史资料' },
        // { url: '/pages/index/newHelp/newHelp', imgUrl: '../../icons/xinshou.png', name: '新手教程' },
        // { url: '/pages/index/stampDraft/stampDraft', imgUrl: '../../icons/shouquan.png', name: '打印委托书' },
        // { url: '/pages/index/bbxr/downLoadBill/downLoadBill?type=2', imgUrl: '../../icons/xiazai.png', name: '下载提货单' },
        // { url: '/pages/index/bbxr/downLoadBill/downLoadBill?type=3', imgUrl: '../../icons/shangchuan.png', name: '上传提货单' },
      ],
      dlr: [
        { url: '/pages/index/bxr/bdataEntry/bdataEntry', imgUrl: '../../icons/bao.png', name: '保险录入' },
        // { url: '/pages/index/dlr/insuranceEntryList/insuranceEntryList', imgUrl: '../../icons/baoxian.png', name: '保险公司代录' },
        { url: '/pages/index/dlr/recordedData/recordedData', imgUrl: '../../icons/lishi.png', name: '已录资料' },
        { url: '/pages/index/newHelp/newHelp', imgUrl: '../../icons/xinshou.png', name: '新手教程' },
      ]
    },
    ywyData: [],
    arr2: [{ id: '1', name: '未递交审核' }, { id: '2', name: '待审核' }, { id: '3', name: '驳回修改' }, { id: '4', name: '已完成' }],
    arr1: [{ id: '0', name: '驳回修改' }, { id: '1', name: '未递交审核' }, { id: '2', name: '代录人未递交' }, { id: '3', name: '代录资料未确认' }, { id: '4', name: '待审核' }, { id: '5', name: '物权人未确认' }, { id: '6', name: '物权人未委托' }, { id: '7', name: '全部资料已完成' }, { id: '8', name: '拍卖已完成' }],
    saleMan1: [],
    saleMan2: [],
    saleManLength: 2,
    duration: 500,
    swiperIndex: 0,
    content:'',
    batch_num:''
  },
  onInput(e){
    var that = this,
        batch_num = e.detail.value,
        content = e.detail.value
    that.setData({batch_num:batch_num,content:content})
  },
  toSearch(){
    this.getSaleManList()
  },
  listInfo(e) {
    // if (e.currentTarget.dataset.type == 0) {
      wx.navigateTo({
        url: './ywy/info/info?p_id=' + e.currentTarget.dataset.pid + '&type=1',
      })
    // }
  },
  getSaleManList() {
    http.getReq('/index/SaleMan/saleManIndex', {
      type: 1,
      batch_num:this.data.batch_num
    }, res => {
      let array2 = res.data
      let array1 = this.data.arr1
      for (var i = 0; i < array2.length; i++) {
        var obj = array2[i];
        var num = obj.policy_status;
        for (var j = 0; j < array1.length; j++) {
          var aj = array1[j];
          var n = aj.id;
          if (n == num) {
            obj.recoder_str = aj.name
          }
        }
      }
      this.setData({
        saleMan1: array2
      })
    })
    http.getReq('/index/SaleMan/saleManIndex', {
      type: 2,
      batch_num:this.data.batch_num
    }, res => {
      let array2 = res.data
      let array1 = this.data.arr2
      for (var i = 0; i < array2.length; i++) {
        var obj = array2[i];
        var num = obj.status;
        for (var j = 0; j < array1.length; j++) {
          var aj = array1[j];
          var n = aj.id;
          if (n == num) {
            obj.recoder_str = aj.name
          }
        }
      }
      this.setData({
        saleMan2: array2
      })
    })
  },
  navigatorUrl(e) {
    console.log(e)
    wx.navigateTo({
      url: e.currentTarget.dataset.url,
    })
  },
  //点击切换active
  changeIndex(e) {
    this.setData({
      swiperIndex: e.currentTarget.dataset.index
    })
  },
  // 列表左右滑动
  intervalChange(e) {
    this.setData({
      swiperIndex: e.detail.current
    })
  },
  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  // 获取高度
  getSwiper() {

    let query = wx.createSelectorQuery()
    wx.getSystemInfo({
      success: res => {
        query.selectAll('.ry_height').boundingClientRect(rect => {
          let heightAll = 0;
          rect.map((currentValue, index, arr) => {
            heightAll = heightAll + currentValue.height

          })
          this.setData({
            scrollheight: res.windowHeight - heightAll
          })
        }).exec();
      }
    });
  },
  onLoad: function () {
    
    // 角色id  1保险人 2 代录人 3被保险人 4业务员
    var character = '', role_name = '';
    switch (wx.getStorageSync('role_id')) {
      case '1':
        role_name = 'bxr';
        character = '理赔员';
        break;
      case '2':
        role_name = 'dlr';
        character = '代录人';
        break;
      case '3':
        role_name = 'bbxr';
        character = '物权人';
        break;
      case '4':
        role_name = 'ywy';
        character = '业务员';
        this.getSaleManList();
        break;
    }
    app.globalData.userInfo.name = role_name;
    this.setData({
        userInfo: this.data.userList[app.globalData.userInfo.name] || [],
        username: wx.getStorageSync('userInfo').username,
        img: wx.getStorageSync('userInfo').img,
        character: character
      });
    this.getUserInfo();
    app.tabBar();
    // app.globalData.time = setInterval(function () {
    //   app.getMessageNum();
    // }, 5000);
    console.log(app)
    this.getSwiper();
  },
  getUserInfo: function (e) {
    console.log(app.globalData)
    http.postReq('/index/user/getUserInfo', {}, res => {
      res.data.role_id = wx.getStorageSync('role_id');
      res.data.img = res.data.head_img;
      app.globalData.userInfo = Object.assign(app.globalData.userInfo, res.data)
      // app.globalData.userInfo = res.data;
      // console.log(app.globalData.userInfo)
      wx.setStorageSync('userInfo', res.data);

    this.setData({
      userInfo: this.data.userList[app.globalData.userInfo.name] || [],
      username: res.data.username,
      company_name: res.data.company_name
    })
  });
  },
  onShow() {
    console.log(app.globalData)
    this.setData({
      username: wx.getStorageSync('userInfo').username,
      img: wx.getStorageSync('userInfo').img,
    });
    setTimeout(function(){
      wx.setNavigationBarTitle({
        title: '融易拍'
      })
    },500)
    
  },
});
