// pages/index/stampDraft/stampDraft.js
var http = require('../../../service/http.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    List: [],
  },
  authorizationTap(e){
    var info = e.target.dataset.item;

    http.getReq('/index/user/getRole', {}, res => {
      if (res.data.data == "1") {
        http.getReq('/index/insurer/insertPrintNum', { policy_num: info.policy_num}, res => {
          console.log(info)
          wx.navigateTo({
            url: '/pages/public/authorizationBook/authorizationBook?name=' + info.rem_name + '&num=' + info.policy_num + '&type=1', //1授权书
          })
        })
      }
      if (res.data.data == "3") {
        http.getReq('/index/insurant/print_report', { policy_num: info.policy_num}, res => {
          console.log(info)
          wx.navigateTo({
            url: '/pages/public/authorizationBook/authorizationBook?set_price=' + res.data.set_price + '&policy_num=' + info.policy_num + '&type=2', //2委托书
          })
        })
      }
    })
  },
  getList() {
    http.getReq('/index/user/getRole', {}, res => {
      var url = "";
      var data = {};
      console.log(res)
      if(res.data.data == "1"){//保险人
        url = '/index/insurer/getAuthReport';
        data = {}
      }
      if(res.data.data == "3") {//物权人
        url = '/index/insurant/download_list';
        data = { type: 1 }
      }

      http.getReq(url, data, _res => {
        this.setData({
          List: _res.data
        })
      });
    })
    // http.getReq('/index/insurant/download_list', {type: 1}, res => {
    // http.getReq('/index/insurer/getAuthReport', {type: 1}, res => {
    //   this.setData({
    //     List: res.data
    //   })
    // });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
