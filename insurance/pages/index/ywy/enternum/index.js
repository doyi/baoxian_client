// pages/index/ywy/enternum/index.js
const app = getApp()
var http = require('../../../../service/http.js')
Page({
  data: {
    policy_num: ''
  },
  one_report_num(e) {
    this.setData({
      policy_num: e.detail.value
    })
  },
  sureStep () {
    if(!this.data.policy_num){
       wx.showToast({title: '请输入报案号'}) ;return;
    }
    http.getReq('/index/SaleMan/saleman_relation', {
      policy_num: this.data.policy_num
    }, res => {
      // 提交
      wx.showToast({
        title: '提交成功',
      })
       setTimeout(function() {
            wx.redirectTo({url:'/pages/index/index'}); 
       }, 800)
    })
  },
  onLoad: function (options) {
    app.tabBar();
  },
  onReady: function () {
  },
  onShow: function () {
  },
  onHide: function () {
  },
  onUnload: function () {
  },
  onPullDownRefresh: function () {
  },
  onReachBottom: function () {
  },
  onShareAppMessage: function () {
  }
})