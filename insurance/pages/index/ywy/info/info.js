// pages/index/bxr/bxrHisData/bxrInfo/bbxrInfo.js
var http = require('../../../../service/http.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    arr2: [{ id: '0', name: '资料未确认' },{ id: '1', name: '未递交审核' }, { id: '2', name: '待审核' }, { id: '3', name: '驳回修改' }, { id: '4', name: '已完成' }],
    arr1: [{ id: '0', name: '驳回修改' }, { id: '1', name: '未递交审核' }, { id: '2', name: '代录人未递交' }, { id: '3', name: '代录资料未确认' }, { id: '4', name: '待审核' }, { id: '5', name: '物权人未确认' }, { id: '6', name: '物权人未委托' }, { id: '7', name: '全部资料已完成' }, { id: '8', name: '拍卖已完成' }],
    goodsInfo: {},
    policy_return: '',
    goods_return: '',
    under_return: '',
    situation_return: '',
    info_return: '',
    certificate_return: '',
    editMsg: '',
    p_id: '',
    type: '1',
    bolDaysList:[],
    bianhao:'',
    showModal: false //自定义弹窗
  },
  download(e){
    http.getReq('/index/NewNeed/downloadAuth', {
      p_id: this.data.p_id
    }, res => {
      console.log(res)
      wx.setClipboardData({
        data: res.data.entrust_url,
        success: function (res) {
          wx.getClipboardData({
            success: function (res) {
              wx.showToast({
                title: '复制成功，请在浏览器打开下载',
                icon: 'none',
                duration: 3000
              })
            }
          })
        }
      })
    })
  },
  editNum(e){
    this.setData({bianhao:e.detail.value})
  },
  save() {
    http.getReq('/index/NewNeed/setNumber', {
      batch_num:this.data.bianhao,
      p_id:this.data.p_id
    }, res => {
      if (res.code==200) {
        wx.showToast({
          title: '提交成功',
        })
        setTimeout(()=>{
          wx.redirectTo({
            url: '/pages/index/index'
          })
        }, 2000)
      } else {
        wx.showToast({
          title: res.msg
        })
      }
      
    })
  },
  getBolDays() {
    http.getReq('/index/insurant/getBolDays', {}, res => {
      this.setData({
        bolDaysList: res.data,
        // newTime: getTime(new Date())
      })
      console.log(this.data.bolDaysList)
    })
  },
  bottonBtn2() {
    http.getReq('/index/insurer/confirmSubmitUpdate', {
      policy_num: this.data.goodsInfo.goods_info.policy_num,
      goods_type: this.data.goodsInfo.goods_info.goods_type,
      goods_name: this.data.goodsInfo.goods_info.goods_name,
      goods_num: this.data.goodsInfo.goods_info.goods_num,
      good_price: this.data.goodsInfo.goods_info.good_price,
      good_pg_price: this.data.goodsInfo.goods_info.good_pg_price,
      damage_at: this.data.goodsInfo.goods_info.damage_at,
      damage_result: this.data.goodsInfo.goods_info.damage_result,
      address: this.data.goodsInfo.goods_info.address,
      address_detail: this.data.goodsInfo.goods_info.address_detail,
      // other_report: this.data.goodsInfo.goods_info.other_report ? this.data.goodsInfo.goods_info.other_report.join(','):'',
      other_report: this.data.goodsInfo.goods_info.other_report,
      present_at: this.data.goodsInfo.goods_info.present_at,
      goods_list: this.data.goodsInfo.goods_info.goods_list,
      // goods_desc: this.data.goodsInfo.goods_info.goods_desc ? this.data.goodsInfo.goods_info.goods_desc.join(','):'',
      goods_desc: this.data.goodsInfo.goods_info.goods_desc,
      present_img: this.data.goodsInfo.goods_info.present_img ? this.data.goodsInfo.goods_info.present_img.join(',') : '',
      present_video: this.data.goodsInfo.goods_info.present_video,
      should_at: this.data.goodsInfo.goods_info.should_at,
      rem_company: this.data.goodsInfo.goods_info.rem_company,
      linkman: this.data.goodsInfo.goods_info.linkman,
      connect_phone: this.data.goodsInfo.goods_info.connect_phone,
    }, res => {
      wx.showToast({
        title: '修改成功',
      })
      setTimeout(() => {
        wx.navigateBack({
          delta: 1
        });
      }, 800);
    })
  },
  tab(e){
    var type = e.currentTarget.dataset.type
    this.setData({
      type:type
    })
    this.getInfo()
  },
  //修改原因
  // editTurn() {
  //   wx.showModal({
  //     title: '提示',
  //     content: '未提交驳回原因，不予提交',
  //   })
  // },
  //修改原因
  // editTurn() {
  //   wx.showModal({
  //     title: '提示',
  //     content: '未提交驳回原因，不予提交',
  //   })
  // },
  preview:function(e){
    var url = e.currentTarget.dataset.src
    console.log(url)
    wx.previewImage({
      current: url, // 当前显示图片的http链接
      urls: [url] // 需要预览的图片http链接列表
    })
  },
  agree () {
    let url = this.data.type == 0 ? '/index/SaleMan/pass_policy' : '/index/SaleMan/pass_insurant'
    let param = {
      // 0
      p_id: this.data.p_id,
      // 1
      i_id: this.data.p_id
    }
    http.postReq(url, param , res => {
      wx.showToast({
        title: '确认成功',
      })
      setTimeout(() => {
        wx.navigateBack({
          delta: 1
        })
      }, 800)
    })
  }, 
  //提交驳回信息
  bohuiTap() {
    let url = this.data.type == 0 ? '/index/SaleMan/returnPolicy' : '/index/SaleMan/returnInurant'
    let param = {
      // 0
      insurance_type: this.data.policy_return,
      good_type: this.data.goods_return,
      damaged_goods: this.data.under_return,
      now_goods: this.data.situation_return,
      rem_info: this.data.info_return,
      author_report: this.data.certificate_return,
      p_id: this.data.p_id,
      // 1
      join_price: this.data.join_price,
      look_goods_info: this.data.look_goods_info,
      bol_info: this.data.bol_info,
      receive_money: this.data.receive_money,
      auth_report: this.data.auth_report,
      i_id: this.data.p_id
    }
    http.postReq(url, param, res => {
      wx.showToast({
        title: '提交成功',
      })
      setTimeout(()=>{
        wx.navigateBack({
          delta: 1
        })
      }, 800)
    })
  },
  hideModal: function () {
    this.setData({
      showModal: false
    });
  },
  onCancel: function () {
    this.hideModal();
  },
  onConfirm: function () {
    console.log(111)
    this.hideModal();
    let params = this.data.edittap
    console.log(params)
    let paramMsg = this.data.editMsg
    this.setData({
      [params]: paramMsg
    })
  },
  edit(e) {
    this.setData({
      showModal: true,
      editMsg: '',
      edittap: e.currentTarget.dataset.bohui
    })
  },
  getEditMsg(e) {
    this.setData({
      editMsg: e.detail.value
    })
  },
  editInfo(e) {
    console.log(e)
    wx.navigateTo({
      url: '/pages/index/bxr/bxrHisData/goodsInfoEdit/goodsInfoEdit?step=' + e.currentTarget.dataset.step + '&info=' + JSON.stringify(e.currentTarget.dataset.info),
    })
  },
  getInfo(e) {
    http.getReq('/index/NewNeed/getInsurerFirst', {
      type: this.data.type,
      p_id: this.data.p_id
    }, res => {
      if (typeof res.data.goods_info.present_video == 'string') {
        res.data.goods_info.present_video = JSON.parse(res.data.goods_info.present_video)
      }
      if (typeof res.data.goods_info.present_img == 'string') {
        res.data.goods_info.present_img = JSON.parse(res.data.goods_info.present_img)
      }

      let arr = [], status = ''
      if (this.data.type == 0) {
        arr = this.data.arr1
        status = res.data.goods_info.policy_status
        // res.data.stataus_str = this.data.arr1[res.data.goods_info.policy_status]
      } else {
        arr = this.data.arr2
        status = res.data.goods_info.status
        // if (status == 3){
        //   res.data.status = 4
        // }
        // res.data.stataus_str = res.data.goods_info.policy_status
      }
      res.data.status = status
      arr.forEach((v,k) => {
        if (parseInt(v.id) == status) {
          res.data.goods_info.stataus_str = v.name
        }
      })
      if (res.data.return_msg) {
        res.data.bohui_info = res.data.return_msg
        if (res.data.status == 0) {
          let policy_return = res.data.return_msg.policy_return
          let goods_return = res.data.return_msg.goods_return
          let under_return = res.data.return_msg.under_return
          let situation_return = res.data.return_msg.situation_return
          let info_return = res.data.return_msg.info_return
          let certificate_return = res.data.return_msg.certificate_return
          this.setData({
            // policy_return: policy_return && policy_return.length > 0 ? policy_return.join(',') : '',
            // goods_return: goods_return && goods_return.length > 0 ? goods_return.join(',') : '',
            // under_return: under_return && under_return.length > 0 ? under_return.join(',') : '',
            // situation_return: situation_return && situation_return.length > 0 ? situation_return.join(',') : '',
            // info_return: info_return && info_return.length > 0 ? info_return.join(',') : '',
            // certificate_return: certificate_return && certificate_return.length > 0 ? certificate_return.join(',') : '',
            policy_return: res.data.return_msg.policy_return,
            goods_return: res.data.return_msg.goods_return,
            under_return: res.data.return_msg.under_return,
            situation_return: res.data.return_msg.situation_return,
            info_return: res.data.return_msg.info_return,
            certificate_return: res.data.return_msg.certificate_return
          })
        }
      }

      res.data.goods_info.address = res.data.goods_info.omit + res.data.goods_info.city + res.data.goods_info.classify 

      var insurance_type = typeof res.data.goods_info.insurance_type == "string" ? res.data.goods_info.insurance_type.split(',') : [];
      var insurance = [];
      insurance_type.map(item => {
        switch (item) {
          case "1":
            insurance.push("企业险");
            break;
          case "2":
            insurance.push("船舶险");
            break;
          case "3":
            insurance.push("责任险");
            break;
          case "4":
            insurance.push("工程险");
            break;
          case "5":
            insurance.push("保证险");
            break;
          case "6":
            insurance.push("家财险");
            break;
          case "7":
            insurance.push("其他险");
            break;
        }
      })
      res.data.goods_info.insurance_type_name = insurance.join(",");

      var goods_type_name = "";
      switch (res.data.goods_info.goods_type){
        case 1:
          goods_type_name = "化工类";
          break;
        case 2:
          goods_type_name = "纤维类";
          break;
        case 3:
          goods_type_name = "农产品类";
          break;
        case 4:
          goods_type_name = "设备类";
          break;
        case 5:
          goods_type_name = "车辆类";
          break;
        case 6:
          goods_type_name = "其他";
          break;
      }

      // if (typeof res.data.goods_info.present_img == 'string'){
      //   res.data.goods_info.present_img = JSON.parse(res.data.goods_info.present_img);
      // }
      // if (typeof res.data.goods_info.present_video == 'string') {
      //   res.data.goods_info.present_video = JSON.parse(res.data.goods_info.present_video);
      // }

      // res.data.status = 0
      console.log(res.data)
      this.setData({
        goodsInfo: res.data,
        bianhao:res.data.batch_num
      })
    })
  },
  editBtn(e) {

    wx.navigateTo({
      url: '../../bdataEntry/bdataEntry?info=' + JSON.stringify(e.currentTarget.dataset.info),
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    this.setData({
      p_id: options.p_id,
      type: options.type
    })
    this.getInfo(options.p_id)
    this.getBolDays()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})