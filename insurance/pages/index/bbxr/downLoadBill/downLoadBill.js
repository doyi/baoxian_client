// pages/index/downLoadBill/downLoadBill.js
var http = require('../../../../service/http.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    type: '',
    hidden: true,
    src: "",
    List: []
  },
  getList() {
    http.getReq('/index/insurant/download_list', { type:this.data.options.type}, res => {
      this.setData({
        List: res.data
      })
    })
  },
  lookHref () {

  },
  upFun (e) {
    var num = e.currentTarget.dataset.num
    console.log(num)
    http.getReq('/index/user/getRole',{},res => {
      wx.navigateTo({
        url: '/pages/public/webView/webView?type=1&phone='+res.data.phone + '&num=' + num, //type=1 上传提货单
      })
    })
    // wx.navigateTo({
    //   url: '/pages/public/webView/webView'
    // });
  },
  cancel () {
    this.setData({
      hidden: true
    })
  },
  downFun (e) {
    var that = this;
    let index = e.currentTarget.dataset.index
    http.postReq('/index/insurant/download_bol', { policy_num: this.data.List[index].policy_num }, res => {
      var sType = res.data.type;
      var bol_url = res.data.bol_url;
      console.log(res)
      if (res.data.bol_url) {
        wx.downloadFile({
          url: res.data.bol_url,
          success(res) {
            console.log(res.tempFilePath)
            if (res.statusCode === 200 || res.statusCode === 404 ) {
              if (sType == "jpg" || sType == "png"){
                that.setData({
                  src: res.tempFilePath,
                  hidden: false
                })
              }else{
                // wx.navigateTo({
                //   url: '/pages/index/bbxr/downLoadBill/show/show?url=' + bol_url
                // })
                wx.openDocument({
                  filePath: res.tempFilePath,
                  fileType: sType,
                  success: function (res) {
                    console.log('打开文档成功')
                  },
                  fail: function () {
                    console.log(res);
                  },
                  complete: function (res) {
                    console.log(res);
                  }
                })
              }
              console.log(res)
            }
          }
        })
      } else {
        wx.showToast({
          title: '暂无提货单',
          icon: 'none'
        })
      }
    })

  },
  onShareAppMessage(ops){
    return {
      title: 'xx小程序',
      path: 'pages/index/index',
      success: function (res) {
        // 转发成功
        console.log("转发成功:" + JSON.stringify(res));
      },
      fail: function (res) {
        // 转发失败
        console.log("转发失败:" + JSON.stringify(res));
      }
    }
  },
  viewImage(e) {
    const { src } = e.target.dataset;
    wx.previewImage({
      urls: [src],
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    // 1 委托书 2 下载 3上传
    // options.type = 3
    if (options.type) {
      if (options.type == 3) {
        wx.setNavigationBarTitle({
          title: '上传提货单'
        });
        // http.getReq('/index/user/getRole',{},_res=>{
        //   http.postReq('/index/upload/get_upload_url',{
        //     phone: _res.data.phone
        //   },res=>{
        //     console.log(res)
        //   })
        // });
      }
      this.setData({
        type: options.type,
        options: options
      })
      this.getList()
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
