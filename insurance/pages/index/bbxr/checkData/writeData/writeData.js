// pages/dataEntry/dataEntry.js
const app = getApp()
var http = require('../../../../../service/http.js');
var getTime = require('../../../../../utils/util.js');
var wechat = require('../../../../../utils/wechat.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    step: 6,
    i_id: '', 
    policy_num: '2',
    // 1
    save_price: '0',
    set_price: '0',
    // 2
    look_type:'',
    look_type_name:'',
    address: '',
    address_detail: '',
    linkman: '',
    phone: '',
    second_phone: '',
    // 3
    is_pay: '1',
    pay_result: '',
    damage_info: '',
    bol_info: '',
    bol_address: '',
    bol_address_detail: '',
    bol_linkman: '',
    bol_phone: '',
    bol_days:'',
    bol_days_name:'',
    // 4
    pay_name: '',
    bank_name: '',
    card_num: '',
    tax_num: '',
    company_address: '',
    // 5
    entrust_url:[],
    is_join_list: [
      { auction: '1', value: '参与', checked: 'true' },
      { auction: '2', value: '不参与' },
    ],
    is_promise_list: [
      { money: '1', value: '申请免交保证金', checked: 'true' },
      { money: '2', value: '交纳保证金' },
    ],
    is_pay_list: [
      { is_payother: '1', value: '是', checked: 'true' },
      { other: '2', value: '否' },
    ],
    lookTypeList: [],
    lookTypeIndex: '',
    bolDaysList: [],
    bolDaysIndex: '',
    pay_result_leng: 0,
    pay_result_max_leng: 200,
    bol_info_length: 0,
    bol_info_max_length: 200,
    checkbox:'1',
    newTime:'',
    chose:''
  },
  nextStep(e) {
    var postData = {};
    console.log(e.target.dataset.index)
    switch (e.target.dataset.index){
      case "6":
        postData = {
          save_price: this.data.save_price,
          set_price: this.data.set_price
        }
      break;
      case "7":
        postData = {
          look_type: this.data.look_type,
          look_type_name: this.data.look_type_name,
          address: this.data.address,
          address_detail: this.data.address_detail,
          linkman: this.data.linkman,
          phone: this.data.phone,
          second_phone: this.data.second_phone
        }
        break;
      case "8":
        postData = {
          is_pay: this.data.is_pay || "1",
          pay_result: this.data.pay_result,
          damage_info: this.data.damage_info,
          bol_info: this.data.bol_info,
          bol_address: this.data.bol_address,
          bol_address_detail: this.data.bol_address_detail,
          bol_linkman: this.data.bol_linkman,
          bol_phone: this.data.bol_phone,
          bol_days: this.data.bol_days,
          bol_days_name: this.data.bol_days_name
        }
        break;
      case "9":
        postData = {
          pay_name: this.data.pay_name,
          bank_name: this.data.bank_name,
          card_num: this.data.card_num,
          tax_num: this.data.tax_num,
          company_address: this.data.company_address
        }
        break;
      case "10":
        // if (!this.data.entrust_url){
        //   wx.showModal({
        //     title: '提示',
        //     content: '授权书为必填项'
        //   });
        //   return;
        // }
        postData = {
          // entrust_url: this.data.entrust_url
          type:this.data.tempType
        }
        break;
    }

    postData.step = this.data.step;
    postData.p_id = this.data.p_id;
    console.log(postData)


    http.postReq('/index/NewNeed/saveInsurer', postData, res => {
      if(e.target.dataset.index == "10"){
        wx.showToast({
          title: res.msg,
          duration: 2000
        })
        setTimeout(() => {
          wx.redirectTo({
            url: '/pages/index/index',
          })
        })
      }else{
        this.setData({
          step: JSON.parse(e.target.dataset.index) + 1
        })
      }
    })

    // if (e.target.dataset.index == '5') {
    //   if (this.data.checkbox){
    //     http.postReq('/index/insurant/saveBolInfo', {
    //       now_status: this.data.step,
    //       i_id: this.data.i_id,
    //       // 1
    //       is_join: this.data.is_join,
    //       is_promise: this.data.is_promise,
    //       back_price: this.data.back_price,
    //       // 2
    //       look_type: this.data.look_type,
    //       look_type_name: this.data.look_type_name,
    //       address: this.data.address,
    //       address_detail: this.data.address_detail,
    //       linkman: this.data.linkman,
    //       phone: this.data.phone,
    //       second_phone: this.data.second_phone,
    //       // 3
    //       is_pay: this.data.is_pay,
    //       pay_result: this.data.pay_result,
    //       bol_info: this.data.bol_info,
    //       bol_address: this.data.bol_address,
    //       bol_address_detail: this.data.bol_address_detail,
    //       bol_linkman: this.data.bol_linkman,
    //       bol_phone: this.data.bol_phone,
    //       bol_days: this.data.bol_days,
    //       bol_days_name: this.data.bol_days_name,
    //       // 4
    //       pay_name: this.data.pay_name,
    //       bank_name: this.data.bank_name,
    //       card_num: this.data.card_num,
    //       tax_num: this.data.tax_num,
    //       company_address: this.data.company_address,
    //       // 5
    //       entrust_url: JSON.stringify(this.data.entrust_url),
    //     }, res => {
    //       wx.showToast({
    //         title: res.msg,
    //         duration: 2000
    //       })
    //       setTimeout(() => {
    //         wx.navigateTo({
    //           url: '../../bbxrHisData/bbxrHisData',
    //         })
    //       })
    //     })
    //   }else{
    //     wx.showToast({
    //       title: '请同意拍卖委托合同',
    //       duration: 2000
    //     })
    //   }
    //   // 提交
    //   return
    // } else {
    //   http.postReq('/index/insurant/saveBolInfo', {
    //     now_status: this.data.step,
    //     i_id: this.data.i_id,
    //     // 1
    //     is_join: this.data.is_join || "1",
    //     is_promise: this.data.is_promise || "1",
    //     back_price: this.data.back_price || "0",
    //     // 2
    //     look_type: this.data.look_type,
    //     look_type_name: this.data.look_type_name,
    //     address: this.data.address,
    //     address_detail: this.data.address_detail,
    //     linkman: this.data.linkman,
    //     phone: this.data.phone,
    //     second_phone: this.data.second_phone,
    //     // 3
    //     is_pay: this.data.is_pay,
    //     pay_result: this.data.pay_result,
    //     bol_info: this.data.bol_info,
    //     bol_address: this.data.bol_address,
    //     bol_address_detail: this.data.bol_address_detail,
    //     bol_linkman: this.data.bol_linkman,
    //     bol_phone: this.data.bol_phone,
    //     bol_days: this.data.bol_days,
    //     bol_days_name: this.data.bol_days_name,
    //     // 4
    //     pay_name: this.data.pay_name,
    //     bank_name: this.data.bank_name,
    //     card_num: this.data.card_num,
    //     tax_num: this.data.tax_num,
    //     company_address: this.data.company_address,
    //     // 5
    //     entrust_url: JSON.stringify(this.data.entrust_url),
    //   }, res => {
    //     this.setData({
    //       step: JSON.parse(e.target.dataset.index) + 1
    //     })
    //   })
    // }
  },
  one_good_save_price(e) {
    this.setData({
      save_price: e.detail.value
    })
  },
  one_good_set_price(e) {
    this.setData({
      set_price: e.detail.value
    })
  },
  one_is_join(e) {
    console.log(e.detail.value)
    this.setData({
      is_join: e.detail.value
    })
  },
  one_is_promise(e) {
    this.setData({
      is_promise: e.detail.value
    })
  },
  one_back_price(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      back_price: e.detail.value
    })
  },

  two_address_detail(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      address_detail: e.detail.value
    })
  },
  two_linkman(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      linkman: e.detail.value
    })
  },
  two_phone(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      phone: e.detail.value
    })
  },
  two_second_phone(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      second_phone: e.detail.value
    })
  },
  radioChange(e) {
    this.setData({
      is_pay: e.detail.value
    })
  },
  three_pay_result(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      pay_result: e.detail.value,
      pay_result_leng: e.detail.value.length,
    })
  },
  three_damage_info(e) {
    this.setData({
      damage_info: e.detail.value
    })
  },
  three_bol_info(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      bol_info: e.detail.value,
      bol_info_leng: e.detail.value.length,
    })
  },
  three_bol_address_detail(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      bol_address_detail: e.detail.value,
    })
  },

  three_bol_linkman(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      bol_linkman: e.detail.value,
    })
  },
  three_bol_phone(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      bol_phone: e.detail.value,
    })
  },
  four_pay_name(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      pay_name: e.detail.value,
    })
  },
  four_bank_name(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      bank_name: e.detail.value,
    })
  },
  four_card_num(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      card_num: e.detail.value,
    })
  },
  four_tax_num(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      tax_num: e.detail.value,
    })
  },
  four_company_address(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      company_address: e.detail.value,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    app.tabBar();
    this.setData({
      p_id: options.p_id,
      step: options.step
    })
    console.log(this.data)
    this.getTemp()
    this.getBolDays()
    this.getLookTypeList();
    this.getInfo(options.p_id)//获取信息
  },
  getTemp(){
    var that = this
    http.getReq('/index/NewNeed/getTemplate', {}, res => {
      this.setData({temp:res.data.data})
    })
  },
  preview:function(e){
    var url = e.currentTarget.dataset.src
    console.log(url)
    wx.previewImage({
      current: url, // 当前显示图片的http链接
      urls: [url] // 需要预览的图片http链接列表
    })
  },
  choseTemp:function(e){
    var that = this,
        idx = e.currentTarget.dataset.idx,
        type = e.currentTarget.dataset.type,
        data = that.data.temp
        for (var i = 0; i < data.length; i++) {
          data[i].checked = false
        }
        data[idx].checked = !data[idx].checked;
        that.setData({
          temp:data,
          tempType:type
        })
  },
  getInfo(e) {
    var that = this
    http.getReq('/index/NewNeed/getInsurerFirst', {
      p_id: e
    }, res => {

        var temp = res.data.template_type,
            tempData = that.data.temp
            if (temp!='') {
              for (var i = 0; i < tempData.length; i++) {
                if (tempData[i].type == temp) {
                  tempData[i].checked = true
                }
              }
            }
            
        that.setData({
          policy_num: res.data.goods_info.policy_num,
          // 1
          set_price: res.data.goods_info.set_price==null?'0':res.data.goods_info.set_price,
          save_price: res.data.goods_info.save_price==null?'0':res.data.goods_info.save_price,
          // 2
          look_type: res.data.goods_info.look_type,
          lookTypeIndex: Number(res.data.goods_info.look_type) - 1,
          look_type_name: res.data.goods_info.look_type_name,
          address: res.data.goods_info.province==''?'':res.data.goods_info.province + ',' + res.data.goods_info.city + ',' + res.data.goods_info.town,
          address_detail: res.data.goods_info.address_detail,
          linkman: res.data.goods_info.linkman,
          phone: res.data.goods_info.phone,
          second_phone: res.data.goods_info.second_phone,
          // 3
          is_pay: res.data.goods_info.is_pay,
          pay_result: res.data.goods_info.pay_result,
          damage_info: res.data.goods_info.damage_info,
          bol_info: res.data.goods_info.bol_info,
          bol_address: res.data.goods_info.bol_province==''?'':res.data.goods_info.bol_province + ',' + res.data.goods_info.bol_city + ',' + res.data.goods_info.bol_town,
          bol_address_detail: res.data.goods_info.bol_address_detail,
          bol_linkman: res.data.goods_info.bol_linkman,
          bol_phone: res.data.goods_info.bol_phone,
          bol_days: res.data.goods_info.bol_days,
          bol_days_name: res.data.goods_info.bol_days_name,
          bolDaysIndex: res.data.goods_info.bol_days,
          // 4
          pay_name: res.data.goods_info.pay_name,
          bank_name: res.data.goods_info.bank_name,
          card_num: res.data.goods_info.card_num,
          tax_num: res.data.goods_info.tax_num,
          company_address: res.data.goods_info.company_address,
          // 5
          entrust_url: res.data.goods_info.entrust_url==''?'':res.data.goods_info.entrust_url,
          temp:tempData,
          tempType:res.data.template_type
          // newTime: getTime(new Date())
        })
      if (res.data.goods_info.is_pay) {

        if (res.data.goods_info.is_pay == '1') {
          that.setData({
            is_pay_list: [
              { other: '1', value: '是', checked: 'true' },
              { other: '2', value: '否' },
            ]
          })
        } else {
          that.setData({
            is_pay_list: [
              { other: '1', value: '是' },
              { other: '2', value: '否', checked: 'true' },
            ]
          })
        }
      }
    })
  },
  // getInfo(e) {
  //   http.getReq('/index/Insurant/getNextBol', {
  //     p_id: e
  //   }, res => {
  //     // if (res.data.goods_info.insurance_type) {
  //     //   for (var i = 0; i < _this.data.insuranceTypeList.length; i++) {
  //     //     console.log(_this.data.insuranceTypeList[i].it_id)
  //     //     console.log(_data.goods_info.insurance_type)
  //     //     if (_this.data.insuranceTypeList[i].it_id == _data.goods_info.insurance_type) {
  //     //       _this.setData({
  //     //         insuranceTypeIndex: i + ''
  //     //       })
  //     //     }
  //     //   }
  //     // }
  //     // if (res.data.goods_info.goods_type) {
  //     //   for (var i = 0; i < _this.data.goodsTypeList.length; i++) {
  //     //     if (_this.data.goodsTypeList[i].it_id == _data.goods_info.goods_type) {
  //     //       _this.setData({
  //     //         goodsTypeIndex: i + ''
  //     //       })
  //     //     }
  //     //   }
  //     // }
  //     this.setData({
  //       policy_num: res.data.goods_info.policy_num,
  //       // 1
  //       set_price: res.data.goods_info.set_price,
  //       save_price: res.data.goods_info.save_price,
  //       // 2
  //       look_type: res.data.goods_info.look_type,
  //       look_type_name: res.data.goods_info.look_type_name,
  //       address: res.data.goods_info.address,
  //       address_detail: res.data.goods_info.address_detail,
  //       linkman: res.data.goods_info.linkman,
  //       phone: res.data.goods_info.phone,
  //       second_phone: res.data.goods_info.second_phone,
  //       // 3
  //       is_pay: res.data.goods_info.is_pay,
  //       pay_result: res.data.goods_info.pay_result,
  //       damage_info: res.data.goods_info.damage_info,
  //       bol_info: res.data.goods_info.bol_info,
  //       bol_address: res.data.goods_info.bol_address,
  //       bol_address_detail: res.data.goods_info.bol_address_detail,
  //       bol_linkman: res.data.goods_info.bol_linkman,
  //       bol_phone: res.data.goods_info.bol_phone,
  //       bol_days: res.data.goods_info.bol_days,
  //       bol_days_name: res.data.goods_info.bol_days_name,
  //       // 4
  //       pay_name: res.data.goods_info.pay_name,
  //       bank_name: res.data.goods_info.bank_name,
  //       card_num: res.data.goods_info.card_num,
  //       tax_num: res.data.goods_info.tax_num,
  //       company_address: res.data.goods_info.company_address,
  //       // 5
  //       entrust_url: res.data.goods_info.entrust_url,
  //       // newTime: getTime(new Date())
  //     })
  //   })
  // },
  //选择省市区
  bindAddressChange(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      address: e.detail.value
    })
  },
  //提货选择省市区
  bindBolAddressChange(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      bol_address: e.detail.value
    })
  },
  //看货要求
  getLookTypeList() {
    http.getReq('/index/insurant/getLookType', {}, res => {
      this.setData({
        lookTypeList: res.data,
        // newTime: getTime(new Date())
      })
    })
  },
  //超时限提货
  getBolDays() {
    http.getReq('/index/insurant/getBolDays', {}, res => {
      this.setData({
        bolDaysList: res.data,
        // newTime: getTime(new Date())
      })
    })
  },
  bolDaysList(e) {
    let typeList = this.data.bolDaysList;
    this.setData({
      bolDaysIndex: e.detail.value,
      bol_days: typeList[e.detail.value].it_id,
      bol_days_name: typeList[e.detail.value].it_id_name
    })
  },
  lookTypeList(e) {
    var idx = Number(e.detail.value);
    let typeList = this.data.lookTypeList;
    this.setData({
      lookTypeIndex: idx,
      look_type: typeList[idx].it_id,
      look_type_name: typeList[idx].it_name,
    })
    
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.setData({
      newTime: getTime.formatTime(new Date())
    })
  },
  preStep(e) {
    this.setData({
      step: JSON.parse(e.target.dataset.index) - 1
    })
  },
  checkboxChange(e) {
    console.log(e)
    this.setData({
      checkbox: e.detail.value[0] || ''
    })
  },
  delAuthorReportFile(e) {
    this.setData({
      entrust_url: ''
    })
  },
  // 授权书

  authorReportFile() {
    wechat.chooseImage()
      .then(d => {
        let { path, size } = d.tempFiles[0];
        // let params = this.data.present_img;
        // params.push(path)
        // this.setData({
        //   tempImagePath: path,
        // });
        // console.log(path)
        return wechat.uploadFile("/index/Upload/index", path, "avatar")
      })
      .then(d => {
        let res = JSON.parse(d.data)
        // let entrust_url = this.data.entrust_url
        // console.log(entrust_url)
        // entrust_url.push(res.data.url)
        // console.log(res.data.ur)
        this.setData({
          entrust_url: [res.data.url]
        })
      })
      .catch(e => {
        console.log(e);
      })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})