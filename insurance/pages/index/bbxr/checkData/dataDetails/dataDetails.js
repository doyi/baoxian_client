// pages/index/checkData/dataDetails/dataDetails.js
var http = require('../../../../../service/http.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info:'',
    p_id:'',
    i_id:'',
    present_img: '["http://img03.tooopen.com/uploadfile/downs/images/20110714/sy_20110714135215645030.jpg","http://img03.tooopen.com/uploadfile/downs/images/20110714/sy_20110714135215645030.jpg"]'
  },

  listInfo(e) {
    wx.navigateTo({
      url: '../writeData/writeData?p_id=' + this.data.p_id + '&i_id=' + this.data.i_id,
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  preview:function(e){
    var url = e.currentTarget.dataset.src
    console.log(url)
    wx.previewImage({
      current: url, // 当前显示图片的http链接
      urls: [url] // 需要预览的图片http链接列表
    })
  },
  getInfo(e) {
    http.getReq('/index/insurant/getGoodInfo', {
      policy_num: e.policy_num
    }, res => {
      console.log(res)
      var goods_type_name = "";
      switch(res.data.goods_type){
        case 1 || "1":
          goods_type_name = "化工类";
          break;
        case 2 || "2":
          goods_type_name = "纤维类";
          break;
        case 3 || "3":
          goods_type_name = "农产品";
          break;
        case 4 || "4":
          goods_type_name = "设备类";
          break;
        case 5 || "5":
          goods_type_name = "其他";
          break;
      }
      res.data.goods_type_name = goods_type_name;
      this.setData({
        p_id:res.data.p_id,
        i_id:e.i_id,
        info: res.data
      })
    })
  },
  onLoad: function (options) {
    console.log(options)
    if (options.i_id && options.policy_num){
      this.getInfo(options)
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})