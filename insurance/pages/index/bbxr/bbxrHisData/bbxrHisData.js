// pages/index/bbxrHisData/bbxrHisData.js
var http = require('../../../../service/http.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hisList:[],
    array: [{id: '5', name: '全部'}, {id: '1', name: '未递交审核'}, {id: '2', name: '待审核'}, {id: '3', name: '驳回修改'}, {id: '4', name: '已完成'}],
    content: '',
    type: 1,
    typeIndex: 0,
    typeName: '全部'
  },
  listInfo(e) {
    console.log(e.currentTarget.dataset.p_id);
    wx.navigateTo({
      url: './info/info?p_id=' + e.currentTarget.dataset.p_id,
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  getHisList() {
    // type 0,驳回修改 ，1未递交审核 2代录人未递交 3代录资料未确认 4待审核 5被保险人未确认 6被保险人未委托 7 全部资料已完成 8 拍卖已完成
    // http.getReq('/index/Insurer/getList',{policy_num: this.data.content, type: this.data.type}, res => {
    //   this.setData({
    //   hisList: res.data
    //  })
    // });
    http.getReq('/index/NewNeed/insurerList', {type: this.data.type}, res => {
      this.setData({
        hisList: res.data
      })
    })
  },
  tab(e){
    var type = e.currentTarget.dataset.type
    http.getReq('/index/NewNeed/insurerList', {type:type}, res => {
      this.setData({
        hisList: res.data,
        type:type
      })
    })
  },
  changeType(e) {
    console.log('changeType', e);
    var typeIndex = Number(e.detail.value);
    var typeName = this.data.array[typeIndex].name;
    var type = this.data.array[typeIndex].id;
    var that = this;
    this.setData({type, typeName, typeIndex}, () => {
      that.getHisList();
    });
  },
  toSearch() {
    this.getHisList();
  },
  onInput(e) {
    this.setData({content: e.detail.value});
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({title: '已录资料'});
    this.getHisList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
