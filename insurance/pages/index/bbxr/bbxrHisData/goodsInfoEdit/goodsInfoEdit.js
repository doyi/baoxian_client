// pages/dataEntry/dataEntry.js
var http = require('../../../../../service/http.js');
var getTime = require('../../../../../utils/util.js');
var wechat = require('../../../../../utils/wechat.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    step: 1,
    i_id: '',
    // 1
    save_price: '',
    set_price: '',
    // 2
    look_type: '',
    address: '',
    address_detail: '',
    linkman: '',
    phone: '',
    second_phone: '',
    // 3
    is_pay: '',
    pay_result: '',
    bol_info: '',
    bol_address: '',
    bol_address_detail: '',
    bol_linkman: '',
    bol_phone: '',
    bol_days: '',
    // 4
    pay_name: '',
    bank_name: '',
    card_num: '',
    tax_num: '',
    company_address: '',
    // 5
    entrust_url: [],
    is_join_list: [
      { auction: '1', value: '参与', checked: 'true' },
      { auction: '2', value: '不参与' },
    ],
    is_promise_list: [
      { money: '1', value: '申请免交保证金', checked: 'true' },
      { money: '2', value: '交纳保证金' },
    ],
    is_pay_list: [
      { other: '1', value: '是', checked: 'true' },
      { other: '2', value: '否' },
    ],
    lookTypeList: [],
    lookTypeIndex: '',
    bolDaysList: [],
    bolDaysIndex: '',
    pay_result_leng: 0,
    pay_result_max_leng: 200,
    bol_info_leng: 0,
    bol_info_max_leng: 200,
    checkbox: '1',
    newTime: ''
  },
  one_good_save_price(e) {
    this.setData({
      save_price: e.detail.value
    })
  },
  one_good_set_price(e) {
    this.setData({
      set_price: e.detail.value
    })
  },
  one_back_price(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      back_price: e.detail.value
    })
  },

  two_address_detail(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      address_detail: e.detail.value
    })
  },
  two_linkman(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      linkman: e.detail.value
    })
  },
  two_phone(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      phone: e.detail.value
    })
  },
  two_second_phone(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      second_phone: e.detail.value
    })
  },
  three_is_pay(e) {
    this.setData({
      is_pay: e.detail.value
    })
  },
  three_pay_result(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      pay_result: e.detail.value,
      pay_result_leng: e.detail.value.length,
    })
  },
  three_bol_info(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      bol_info: e.detail.value,
      bol_info_leng: e.detail.value.length,
    })
  },
  three_bol_address_detail(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      bol_address_detail: e.detail.value,
    })
  },

  three_bol_linkman(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      bol_linkman: e.detail.value,
    })
  },
  three_bol_phone(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      bol_phone: e.detail.value,
    })
  },
  four_pay_name(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      pay_name: e.detail.value,
    })
  },
  four_bank_name(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      bank_name: e.detail.value,
    })
  },
  four_card_num(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      card_num: e.detail.value,
    })
  },
  four_tax_num(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      tax_num: e.detail.value,
    })
  },
  four_company_address(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      company_address: e.detail.value,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getBolDays()
    this.getLookTypeList()

    this.setData({
      step: options.step,
    });
    let _this = JSON.parse(options.info)
    let that = this
    console.log(_this)
    setTimeout(function () {
      if (_this.res.look_type) {
        var lookTypeIndex=Number(_this.res.look_type) - 1
        var it_name = that.data.lookTypeList[lookTypeIndex].it_name
        that.setData({
          it_name:it_name
        })
      }
      if (_this.res.is_join) {

        if (_this.res.is_join == '1') {
          that.setData({
            is_join_list: [
              { auction: '1', value: '参与', checked: 'true' },
              { auction: '2', value: '不参与' },
            ],
          })
        } else {
          that.setData({
            is_join_list: [
              { auction: '1', value: '参与' },
              { auction: '2', value: '不参与', checked: 'true' },
            ],
          })
        }
      }
      if (_this.res.is_pay) {

        if (_this.res.is_pay == '1') {
          that.setData({
            is_pay_list: [
              { other: '1', value: '是', checked: 'true' },
              { other: '2', value: '否' },
            ]
          })
        } else {
          that.setData({
            is_pay_list: [
              { other: '1', value: '是' },
              { other: '2', value: '否', checked: 'true' },
            ]
          })
        }
      }
    },1000)
    this.setData({
      // 1
      set_price: _this.res.set_price,
      save_price: _this.res.save_price,
      // 2
      look_type: _this.res.look_type,
      look_type_name: _this.res.look_type_name,
      address: _this.res.address.split(','),
      address_detail: _this.res.address_detail,
      linkman: _this.res.linkman,
      phone: _this.res.phone,
      second_phone: _this.res.second_phone,
      // 3
      is_pay: _this.res.is_pay,
      pay_result: _this.res.pay_result,
      damage_info: _this.res.damage_info,
      bol_info: _this.res.bol_info,
      bol_address: _this.res.bol_address.split(','),
      bol_address_detail: _this.res.bol_address_detail,
      bol_linkman: _this.res.bol_linkman,
      bol_phone: _this.res.bol_phone,
      bol_days: _this.res.bol_days,
      bolDaysIndex: _this.res.bol_days==null?'':_this.res.bol_days,
      // 4
      pay_name: _this.res.pay_name,
      bank_name: _this.res.bank_name,
      card_num: _this.res.card_num,
      tax_num: _this.res.tax_num,
      company_address: _this.res.company_address,
      // 5
      policy_num: _this.res.policy_num,
      entrust_url: _this.res.entrust_url
    })
    console.log(this.data)

  },
  //选择省市区
  bindAddressChange(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      address: e.detail.value
    })
  },
  //提货选择省市区
  bindBolAddressChange(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      bol_address: e.detail.value
    })
  },
  //看货要求
  getLookTypeList() {
    http.getReq('/index/insurant/getLookType', {}, res => {
      this.setData({
        lookTypeList: res.data,
        // newTime: getTime(new Date())
      })
    })
  },
  //超时限提货
  getBolDays() {
    http.getReq('/index/insurant/getBolDays', {}, res => {
      this.setData({
        bolDaysList: res.data,
        // newTime: getTime(new Date())
      })
    })
  },
  bolDaysList(e) {
    let typeList = this.data.bolDaysList;
    this.setData({
      bolDaysIndex: e.detail.value,
      bol_days: typeList[e.detail.value].it_id
    })
  },
  lookTypeList(e) {
    console.log(e)
    let typeList = this.data.lookTypeList;
    this.setData({
      lookTypeIndex: e.detail.value,
      look_type: typeList[e.detail.value].it_id,
      look_type_name: typeList[e.detail.value].it_name,
      it_name:typeList[e.detail.value].it_name
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    // this.setData({
    //   newTime: getTime(new Date())
    // })
  },
  nextStep(e) {

    let pages = getCurrentPages();//当前页面    
    let prevPage = pages[pages.length - 2];//上一页面\

    let is_join = 'goodsInfo.res.is_join'
    let is_promise = 'goodsInfo.res.is_promise'
    let back_price = 'goodsInfo.res.back_price'

    let look_type = 'goodsInfo.res.look_type'
    let look_type_name = 'goodsInfo.res.look_type_name'
    let connect_phone = 'goodsInfo.res.connect_phone'
    let address = 'goodsInfo.res.address'
    let address_detail = 'goodsInfo.res.address_detail'
    let linkman = 'goodsInfo.res.linkman'
    let phone = 'goodsInfo.res.phone'
    let second_phone = 'goodsInfo.res.second_phone'

    let pay_name = 'goodsInfo.res.pay_name'
    let bank_name = 'goodsInfo.res.bank_name'
    let card_num = 'goodsInfo.res.card_num'
    let tax_num = 'goodsInfo.res.tax_num'
    let company_address = 'goodsInfo.res.company_address'

    if (this.data.step == '1') {
      prevPage.setData({
        set_price: this.data.set_price,
        save_price: this.data.save_price
      })
    } else if (this.data.step == '2') {
      prevPage.setData({
        look_type: this.data.look_type,
        look_type_name: this.data.look_type_name,
        address: this.data.address,
        address_detail: this.data.address_detail,
        linkman: this.data.linkman,
        phone: this.data.phone,
        second_phone: this.data.second_phone
      })
    } else if (this.data.step == '3') {
      prevPage.setData({
        is_pay: this.data.is_pay,
        pay_result: this.data.pay_result,
        damage_info: this.data.damage_info,
        bol_info: this.data.bol_info,
        bol_address: this.data.bol_address,
        bol_address_detail: this.data.bol_address_detail,
        bol_linkman: this.data.bol_linkman,
        bol_phone: this.data.bol_phone,
        bol_days: this.data.bol_days,
      })
    } else if (this.data.step == '4') {
      prevPage.setData({
        pay_name: this.data.pay_name,
        bank_name: this.data.bank_name,
        card_num: this.data.card_num,
        tax_num: this.data.tax_num,
        company_address: this.data.company_address,
      })
    } else if (this.data.step == '5') {
      prevPage.setData({
        policy_num: this.data.policy_num,
        set_price: this.data.set_price,
        entrust_url: this.data.entrust_url
      })
    }

    wx.navigateBack();
  },
  preStep(e) {
    this.setData({
      step: JSON.parse(e.target.dataset.index) - 1
    })
  },
  checkboxChange(e) {
    console.log(e)
    this.setData({
      checkbox: e.detail.value[0] || ''
    })
  },
  delAuthorReportFile(e) {
    var entrust_url = this.data.entrust_url;
    var arr = [];
    for (var i = 0; i < entrust_url.length; i++) {
      console.log(e.currentTarget.dataset.index)
      if (i == e.currentTarget.dataset.index) {
      } else {
        arr.push(entrust_url[i])
      }
    }
    // imgList = 
    this.setData({
      entrust_url: arr
    })
  },
  // 授权书

  authorReportFile() {
    wechat.chooseImage()
      .then(d => {
        let { path, size } = d.tempFiles[0];
        // let params = this.data.present_img;
        // params.push(path)
        // this.setData({
        //   tempImagePath: path,
        // });
        // console.log(path)
        return wechat.uploadFile("/index/Upload/index", path, "avatar")
      })
      .then(d => {
        let res = JSON.parse(d.data)
        // entrust_url.push(res.data.url)
        // console.log(entrust_url)
        this.setData({
          entrust_url: res.data.url
        })
      })
      .catch(e => {
        console.log(e);
      })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return {
      title: '融易拍',
      path: '/pages/login/login/login'
    }
  }
})