// pages/index/bxr/bxrHisData/bxrInfo/bbxrInfo.js
var http = require('../../../../../service/http.js')
var wechat = require('../../../../../utils/wechat.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsInfo: {},
    set_price:'',
    save_price:'',
    look_type: '',
    look_type_name: '',
    connect_phone: '',
    address: '',
    address_detail: '',
    linkman: '',
    phone: '',
    second_phone: '',
    is_pay: '',
    pay_result: '',
    damage_info: '',
    bol_info: '',
    bol_address: '',
    bol_address_detail: '',
    bol_linkman: '',
    bol_phone: '',
    bol_days: '',
    bolDaysList:[],
    pay_name: '',
    bank_name: '',
    card_num: '',
    tax_num: '',
    company_address: '',
    entrust_url:'',
    type:1
  },
  editInfo(e) {
    console.log(e)
    wx.navigateTo({
      url: '../goodsInfoEdit/goodsInfoEdit?step=' + e.currentTarget.dataset.step + '&info=' + JSON.stringify(e.currentTarget.dataset.info),
    })
  },
  editBtn(e) {
    console.log(this.data.goodsInfo)
    wx.navigateTo({
      url: '../../checkData/writeData/writeData?i_id=' + e.currentTarget.dataset.i_id + '&p_id=' + this.data.goodsInfo.res.p_id,
    })
  },
  download(e){
    http.getReq('/index/NewNeed/downloadAuth', {
      p_id: this.data.p_id
    }, res => {
      console.log(res)
      wx.setClipboardData({
        data: res.data.entrust_url,
        success: function (res) {
          wx.getClipboardData({
            success: function (res) {
              wx.showToast({
                title: '复制成功，请在浏览器打开下载',
                icon: 'none',
                duration: 3000
              })
            }
          })
        }
      })
    })
  },
  tab(e){
    var type = e.currentTarget.dataset.type
    this.setData({
      type:type
    })
    this.getInfo()
  },
  getInfo(e) {
    http.getReq('/index/NewNeed/getInsurerFirst', {
      p_id: this.data.p_id,
      type:this.data.type
    }, res => {
      console.log(res)
      // var insurance_type = typeof res.data.insurance_type == "string" ?res.data.insurance_type.split(',') : [];
      // var insurance = [];
      // console.log(typeof res.data.insurance_type == "string", insurance_type)
      // insurance_type.map(item => {
      //   console.log(item)
      //   switch(item){
      //     case "1":
      //       insurance.push("企业险");
      //       break;
      //     case "2":
      //       insurance.push("船舶险");
      //       break;
      //     case "3":
      //       insurance.push("责任险");
      //       break;
      //     case "4":
      //       insurance.push("工程险");
      //       break;
      //     case "5":
      //       insurance.push("保证险");
      //       break;
      //     case "6":
      //       insurance.push("家财险");
      //       break;
      //     case "7":
      //       insurance.push("其他险");
      //       break;
      //   }
      // })
      // res.data.insurance_type_name = insurance.join(",");
      // switch(res.data.res.look_type) {
      //   case 1:
      //     res.data.res.look_type_name = "预约联系";
      //     break;
      //   case 2:
      //     res.data.res.look_type_name = "随时看货";
      //     break;
      //   case 3:
      //     res.data.res.look_type_name = "交纳保证金后看货";
      //     break;
      //   default:
      //     break;
      // }
      console.log(res.data);
      var goodsInfo = res.data
      // goodsInfo.res.address = res.data.res.province + ',' + res.data.res.city + ',' + res.data.res.town
      // goodsInfo.res.bol_address = res.data.res.bol_province + ',' + res.data.res.bol_city + ',' + res.data.res.bol_town
      this.setData({
        goodsInfo: goodsInfo,
        // newTime: getTime(new Date())
      })
    })
  },
  bottonBtn2() {
    http.postReq('/index/insurant/saveBolInfo', {
      now_status: this.data.step,
      i_id: this.data.goodsInfo.res.i_id,
      // 1
      set_price: this.data.goodsInfo.res.set_price,
      save_price: this.data.goodsInfo.res.save_price,
      // 2
      look_type: this.data.goodsInfo.res.look_type,
      look_type_name: this.data.goodsInfo.res.look_type_name,
      address: this.data.goodsInfo.res.address,
      address_detail: this.data.goodsInfo.res.address_detail,
      linkman: this.data.goodsInfo.res.linkman,
      phone: this.data.goodsInfo.res.phone,
      second_phone: this.data.goodsInfo.res.second_phone,
      // 3
      is_pay: this.data.goodsInfo.res.is_pay,
      pay_result: this.data.goodsInfo.res.pay_result,
      damage_info: this.data.goodsInfo.res.damage_info,
      bol_info: this.data.goodsInfo.res.bol_info,
      bol_address: this.data.goodsInfo.res.bol_address,
      bol_address_detail: this.data.goodsInfo.res.bol_address_detail,
      bol_linkman: this.data.goodsInfo.res.bol_linkman,
      bol_phone: this.data.goodsInfo.res.bol_phone,
      bol_days: this.data.goodsInfo.res.bol_days,
      bolDaysIndex: this.data.goodsInfo.res.bol_days,
      // 4
      pay_name: this.data.goodsInfo.res.pay_name,
      bank_name: this.data.goodsInfo.res.bank_name,
      card_num: this.data.goodsInfo.res.card_num,
      tax_num: this.data.goodsInfo.res.tax_num,
      company_address: this.data.goodsInfo.res.company_address,
      // 5
      policy_num: this.data.goodsInfo.res.policy_num,
      entrust_url: this.data.goodsInfo.res.entrust_url,
      now_status:5
    }, res => {
      wx.showToast({
        title: res.msg,
        duration: 2000
      })
      setTimeout(() => {
        wx.navigateTo({
          url: '/pages/index/index',
        })
      },2000)
    })
  },
  //超时限提货
  getBolDays() {
    http.getReq('/index/insurant/getBolDays', {}, res => {
      this.setData({
        bolDaysList: res.data,
        // newTime: getTime(new Date())
      })
      console.log(this.data.bolDaysList)
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({p_id:options.p_id})
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  getNow(){
    var that = this;
    that.getBolDays()
    that.getInfo()
    setTimeout(function(){
        var goodsInfo = that.data.goodsInfo;
        var set_price = that.data.set_price,
        save_price = that.data.save_price,
        look_type = that.data.look_type,
        look_type_name = that.data.look_type_name,
        address = that.data.address,
        address_detail = that.data.address_detail,
        linkman = that.data.linkman,
        phone = that.data.phone,
        second_phone = that.data.second_phone,
        is_pay = that.data.is_pay,
        pay_result = that.data.pay_result,
        damage_info = that.data.damage_info,
        bol_info = that.data.bol_info,
        bol_address = that.data.bol_address,
        bol_address_detail = that.data.bol_address_detail,
        bol_linkman = that.data.bol_linkman,
        bol_phone = that.data.bol_phone,
        bol_days = that.data.bol_days,
        pay_name = that.data.pay_name,
        bank_name = that.data.bank_name,
        card_num = that.data.card_num,
        tax_num = that.data.tax_num,
        company_address = that.data.company_address,
        entrust_url = that.data.entrust_url;

        if (entrust_url !='') {
          goodsInfo.res.entrust_url = entrust_url
        };

        if (set_price !='') {
          goodsInfo.res.set_price = set_price
        };

        if (set_price !='') {
          goodsInfo.res.set_price = set_price
        };
        if (save_price !='') {
          goodsInfo.res.save_price = save_price
        };
        if (look_type_name !='') {
          goodsInfo.res.look_type_name = look_type_name
        };
        if (look_type !='') {
          console.log(look_type)
          goodsInfo.res.look_type = look_type
        };
        if (address !='') {
          console.log(address)
          goodsInfo.res.address = address.join(',')
          goodsInfo.res.province = address[0]
          goodsInfo.res.city = address[1]
          goodsInfo.res.town = address[2]
        };
        if (address_detail !='') {
          goodsInfo.res.address_detail = address_detail
        };
        if (linkman !='') {
          goodsInfo.res.linkman = linkman
        };
        if (phone !='') {
          goodsInfo.res.phone = phone
        };
        if (second_phone !='') {
          goodsInfo.res.second_phone = second_phone
        };

        if (is_pay !='') {
          goodsInfo.res.is_pay = is_pay
        };

        if (pay_result !='') {
          goodsInfo.res.pay_result = pay_result
        };

        if (damage_info !='') {
          goodsInfo.res.damage_info = damage_info
        };

        if (bol_info !='') {
          goodsInfo.res.bol_info = bol_info
        };

        if (bol_address !='') {
          goodsInfo.res.bol_address = bol_address.join(',')
          goodsInfo.res.bol_province = bol_address[0]
          goodsInfo.res.bol_city = bol_address[1]
          goodsInfo.res.bol_town = bol_address[2]
        };

        if (bol_address_detail !='') {
          goodsInfo.res.bol_address_detail = bol_address_detail
        };

        if (bol_linkman !='') {
          goodsInfo.res.bol_linkman = bol_linkman
        };

        if (bol_phone !='') {
          goodsInfo.res.bol_phone = bol_phone
        };

        if (bol_days !='') {
          goodsInfo.res.bol_days = bol_days
        };

        if (pay_name !='') {
          goodsInfo.res.pay_name = pay_name
        };

        if (bank_name !='') {
          goodsInfo.res.bank_name = bank_name
        };

        if (card_num !='') {
          goodsInfo.res.card_num = card_num
        };

        if (tax_num !='') {
          goodsInfo.res.tax_num = tax_num
        };

        if (company_address !='') {
          goodsInfo.res.company_address = company_address
        };


        that.setData({goodsInfo:goodsInfo})
        console.log(that.data.goodsInfo)
    },300)
  },
  preview:function(e){
    var url = e.currentTarget.dataset.src
    console.log(url)
    wx.previewImage({
      current: url, // 当前显示图片的http链接
      urls: [url] // 需要预览的图片http链接列表
    })
  },
  onShow: function () {
    var that = this;
    that.getNow()
  },

  authorReportFile() {
    wechat.chooseImage()
      .then(d => {
        let { path, size } = d.tempFiles[0];
        // let params = this.data.present_img;
        // params.push(path)
        // this.setData({
        //   tempImagePath: path,
        // });
        console.log(path)
        return wechat.uploadFile("/index/Upload/index", path, "avatar")
      })
      .then(d => {
        let res = JSON.parse(d.data)
        console.log(res)
        http.getReq('/index/NewNeed/saveDownloadAuth', {
          p_id: this.data.p_id,
          entrust_url:res.data.url
        }, resp => {
          wx.showToast({
            title: '上传成功',
            icon: 'success',
            duration: 2000
          })
          this.getNow()
        })
        // let entrust_url = this.data.entrust_url
        // console.log(entrust_url)
        // entrust_url.push(res.data.url)
        // console.log(res.data.ur)
        // this.setData({
        //   entrust_url: [res.data.url]
        // })
      })
      .catch(e => {
        console.log(e);
      })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})