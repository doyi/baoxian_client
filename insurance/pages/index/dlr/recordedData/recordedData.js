// pages/my/recordedData/recordedData.js
var http = require('../../../../service/http.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hisList: [],
    array: [],
    content: '',
    type: '1',
    typeIndex: 0,
    typeName: '全部'
  },

  listInfo(e) {
    wx.navigateTo({
      url: './info/info?p_id=' + e.currentTarget.dataset.p_id,
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },
  getHisList() {
    console.log(this.data.type)
    http.getReq('/index/NewNeed/insurerList', { type: this.data.type}, res => {
      this.setData({
        hisList: res.data
      })
    })
  },
  tab(e){
    var type = e.currentTarget.dataset.type
    http.getReq('/index/NewNeed/insurerList', {type:type}, res => {
      this.setData({
        hisList: res.data,
        type:type
      })
    })
  },
  changeType(e) {
    console.log('changeType', e);
    var typeIndex = Number(e.detail.value);
    var typeName = this.data.array[typeIndex].name;
    var type = this.data.array[typeIndex].id;
    var that = this;
    this.setData({type, typeName, typeIndex}, () => {
      that.getHisList();
    });
  },
  toSearch() {
    this.getHisList();
  },
  onInput(e) {
    this.setData({content: e.detail.value});
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({title: '已录资料'});
    this.getHisList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    let arr = []
    switch (parseInt(wx.getStorageSync('role_id'))) {
      // 保险人
      case 1 :
        arr = [{ id: '-1', name: '全部' }, { id: '0', name: '驳回修改' }, { id: '1', name: '未递交审核' }, { id: '2', name: '代录人未递交' }, { id: '3', name: '代录资料未确认' }, { id: '4', name: '待审核' }, { id: '5', name: '物权人未确认' }, { id: '6', name: '物权人未委托' }, { id: '7', name: '全部资料已完成' }, { id: '8', name: '拍卖已完成' }]
        break;
      // 代路人
      case 2 :
        arr = [{ id: '-1', name: '全部' }, { id: '1', name: '未递交' }, { id: '2', name: '已提交保险公司人员未确认' }, { id: '3', name: '驳回' }, { id: '4', name: '已完成' }]
        break;
      // 物权人
      case 3:
        arr = [{ id: '-1', name: '全部' }, { id: '1', name: '未递交审核' }, { id: '2', name: '待审核' }, { id: '3', name: '驳回修改' }, { id: '4', name: '已完成' }]
        break;
      // 业务员
      case 4:
        arr = []
        break;
    }
    this.setData({
      array: arr
    })
    this.getHisList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
