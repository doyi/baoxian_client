// pages/index/bxr/bxrHisData/bxrInfo/bbxrInfo.js
var http = require('../../../../../service/http.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goodsInfo: {},
    policy_return: '',
    goods_return: '',
    under_return: '',
    situation_return: '',
    info_return: '',
    certificate_return: '',
    editMsg: '',
    p_id: '',
    type:1,
    bolDaysList:[],
    role_id:wx.getStorageSync('role_id'),
    showModal: false //自定义弹窗
  },
  preview:function(e){
    var url = e.currentTarget.dataset.src
    console.log(url)
    wx.previewImage({
      current: url, // 当前显示图片的http链接
      urls: [url] // 需要预览的图片http链接列表
    })
  },
  tab(e){
    var type = e.currentTarget.dataset.type
    this.setData({
      type:type
    })
    this.getInfo()
  },
  bottonBtn2() {
    var arr = this.data.goodsInfo.goods_list
    //   //sconsole.log(arr.length)
    //   console.log('1111')

    // var arrData = []
    // for (var i = 0; i < arr.length; i++) {
    //   var obj={}
    //   obj.name = arr[i].name
    //   obj.url = arr[i].url
    //   arrData.push(obj)
    // };
    var ads = this.data.address,
        newsAds = ads.split(',');
        console.log(newsAds)
    var province = newsAds[0],
        city = newsAds[1],
        town = newsAds[2];
    var pram = {
      policy_num: this.data.goodsInfo.goods_info.policy_num,
      goods_type: this.data.goodsInfo.goods_info.goods_type,
      goods_name: this.data.goodsInfo.goods_info.goods_name,
      goods_num: this.data.goodsInfo.goods_info.goods_num,
      good_price: this.data.goodsInfo.goods_info.good_price,
      good_pg_price: this.data.goodsInfo.goods_info.good_pg_price,
      damage_at: this.data.goodsInfo.goods_info.damage_at,
      damage_result: this.data.goodsInfo.goods_info.damage_result,
      province: province,
      city: city,
      town: town,
      address_detail: this.data.goodsInfo.goods_info.address_detail,
      other_report: JSON.stringify(this.data.goodsInfo.goods_info.other_report),
      present_at: this.data.goodsInfo.goods_info.present_at,
      goods_list: JSON.stringify(this.data.goodsInfo.goods_info.goods_list),
      goods_desc:  this.data.goodsInfo.goods_info.goods_desc,
      present_img: JSON.stringify(this.data.goodsInfo.goods_info.present_img),
      present_video: JSON.stringify(this.data.goodsInfo.goods_info.present_video),
      should_at: this.data.goodsInfo.goods_info.should_at,
      rem_company: this.data.goodsInfo.goods_info.rem_company,
      linkman: this.data.goodsInfo.goods_info.linkman,
      connect_phone: this.data.goodsInfo.goods_info.connect_phone,
      now_status:10,
    }
    console.log(pram)

    http.postReq('/index/recordor/recordAdd', pram, res => {
          wx.showToast({title: '修改成功',})
          setTimeout(() => {
            wx.navigateTo({
              url: '/pages/index/index',
            })
          }, 800);
    })
  },
  //修改原因
  // editTurn() {
  //   wx.showModal({
  //     title: '提示',
  //     content: '未提交驳回原因，不予提交',
  //   })
  // },
  //修改原因
  // editTurn() {
  //   wx.showModal({
  //     title: '提示',
  //     content: '未提交驳回原因，不予提交',
  //   })
  // },
  //提交驳回信息
  bohuiTap() {
    http.getReq('/index/Insurer/reject', {
      p_id: this.data.p_id,
      policy_return: this.data.policy_return,
      goods_return: this.data.goods_return,
      under_return: this.data.under_return,
      situation_return: this.data.situation_return,
      info_return: this.data.info_return,
      certificate_return: this.data.certificate_return,
    }, res => {
      wx.showToast({
        title: res.msg,
      })
      // newTime: getTime(new Date())
    })
  },
  hideModal: function () {
    this.setData({
      showModal: false
    });
  },
  onCancel: function () {
    this.hideModal();
  },
  onConfirm: function () {
    console.log(111)
    this.hideModal();
    let params = this.data.edittap
    console.log(params)
    let paramMsg = this.data.editMsg
    this.setData({
      [params]: paramMsg
    })
  },
  edit(e) {
    this.setData({
      showModal: true,
      editMsg: '',
      edittap: e.currentTarget.dataset.bohui
    })
  },
  getEditMsg(e) {
    this.setData({
      editMsg: e.detail.value
    })
  },
  editInfo(e) {
    console.log(this.data.address)
    console.log(this.data.goodsInfo)
    var ads = this.data.address,
        newsAds = ads.split(',');
        console.log(newsAds)
    var omit = newsAds[0],
        city = newsAds[1],
        classify = newsAds[2],
        goodsInfo = this.data.goodsInfo;

    goodsInfo.goods_info.omit = omit
    goodsInfo.goods_info.city = city
    goodsInfo.goods_info.classify = classify
    console.log(goodsInfo)
    this.setData({goodsInfo:goodsInfo})
    
    setTimeout(function (){
      wx.navigateTo({
        url: '../goodsInfoEdit/goodsInfoEdit?step=' + e.currentTarget.dataset.step + '&info=' + JSON.stringify(goodsInfo),
      })
    },1000)
    
  },
  getInfo() {
    http.getReq('/index/NewNeed/getInsurerFirst', {
      p_id: this.data.p_id,
      type: this.data.type
    }, res => {
      console.log(res)
      if (this.data.type==1) {
        if (typeof res.data.goods_info.present_video == 'string') {
          res.data.goods_info.present_video = res.data.goods_info.present_video.split(',')
        }
        if (typeof res.data.goods_info.present_img == 'string') {
          res.data.goods_info.present_img = res.data.goods_info.present_img.split(',')
        }

        if(typeof res.data.goods_info.other_report == 'string') {
          res.data.goods_info.other_report = res.data.goods_info.other_report.split(',')
        }

        let address = '';
        if (res.data.goods_info.omit != '' || res.data.goods_info.city!='' || res.data.goods_info.classify!='') {
          address = res.data.goods_info.omit + ',' + res.data.goods_info.city + ',' + res.data.goods_info.classify
        };
        this.setData({
          goodsInfo: res.data,
          address:address 
          // newTime: getTime(new Date())
        })
      };

      if (this.data.type == 2) {
        var goodsInfo = res.data
        goodsInfo.goods_info.address = res.data.goods_info.province + ',' + res.data.goods_info.city + ',' + res.data.goods_info.town
        goodsInfo.goods_info.bol_address = res.data.goods_info.bol_province + ',' + res.data.goods_info.bol_city + ',' + res.data.goods_info.bol_town
        goodsInfo.goods_info.bol_days = Number(goodsInfo.goods_info.bol_days)

        this.setData({
          goodsInfo: res.data
        })
      };
      console.log(this.data.goodsInfo)
    })
  },
  //超时限提货
  getBolDays() {
    http.getReq('/index/insurant/getBolDays', {}, res => {
      this.setData({
        bolDaysList: res.data,
        // newTime: getTime(new Date())
      })
      console.log(this.data.bolDaysList)
    })
  },
  editBtn(e) {
    var step = e.currentTarget.dataset.step
    console.log(step)
    if (step>=1 && step<=5) {
      wx.navigateTo({
        url: '../../dataEntry/dataEntry?p_id=' + this.data.p_id + '&step=' + step + '&type=1',
      })
    };
    if (step>=6 && step<=10) {
      wx.navigateTo({
        url: '../../../bbxr/checkData/writeData/writeData?p_id=' + this.data.p_id + '&step=' + step + '&type=2',
      })
    };
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    this.setData({
      p_id: options.p_id
    })
    console.log(options.p_id)
    this.getInfo()
    console.log(this.data.goodsInfo)
    this.getBolDays()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    console.log(this.data.goodsInfo)

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})