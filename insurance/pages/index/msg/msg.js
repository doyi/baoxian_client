// pages/my/myMsg/myMsg.js
var http = require('../../../service/http.js');
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    msgList: [],
    height: 0,
    scrollY: true,
    content: '',
    landed: false,
  },
  swipeCheckX: 35, //激活检测滑动的阈值
  swipeCheckState: 0, //0未激活 1激活
  maxMoveLeft: 50, //消息列表项最大左滑距离
  correctMoveLeft: 50, //显示菜单时的左滑距离
  thresholdMoveLeft: 30,//左滑阈值，超过则显示菜单
  lastShowMsgId: '', //记录上次显示菜单的消息id
  moveX: 0,  //记录平移距离
  showState: 0, //0 未显示菜单 1显示菜单
  touchStartState: 0, // 开始触摸时的状态 0 未显示菜单 1 显示菜单
  swipeDirection: 0, //是否触发水平滑动 0:未触发 1:触发水平滑动 2:触发垂直滑动
  onInput(e) {
    this.setData({content: e.detail.value});
  },
  toSearch() {
    this.getMsgList(100);
    console.log('toSearch');
    // this.getHisList();
  },
  getMsgList(type) {
    // type 0,驳回修改 ，1未递交审核 2代录人未递交 3代录资料未确认 4待审核 5被保险人未确认 6被保险人未委托 7 全部资料已完成 8 拍卖已完成
    let url = type == 100 ? '/index/Message/search_msg' : '/index/Message/getMessList'
    http.getReq(url, { type: 1, keyword: this.data.content}, res => {
      if (JSON.stringify(res.data) != "{}"){
        var arr = res.data.data.reverse();
      }else{
        var arr = [];
      }
      
      this.setData({
        msgList: arr,
        landed: true
      })
      app.setMessageNum(arr.length)
    });
  },
  onDeleteMsgTap(e){
    /* 一片天空修改  未定义index  start*/
    var index = e.currentTarget.dataset.index;
    /* 一片天空修改  未定义index  end*/
    let mn_id = e.currentTarget.dataset.info.mn_id
    http.getReq('/index/Message/del_msg',{
      mn_id: mn_id
    }, res => {
      wx.showToast({
        title: res.msg,
        icon:'none',
      })
      this.getMsgList()
      this.translateXMsgItem(index, 0, 200)
    })
  },
  upMessageStatus(e){
    let info = e.currentTarget.dataset.info
    if (info.mn_status === '2') return false;
    http.getReq('/index/Message/upMessageStatus',{
      ids: info.mn_id
    }, res => {
      wx.showToast({
        title: res.msg,
        icon:'none',
      })
      this.getMsgList()
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({title: '消息'});
    app.tabBar();

    this.getMsgList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  ontouchstart: function (e) {
    if (this.showState === 1) {
      this.touchStartState = 1;
      this.showState = 0;
      this.moveX = 0;
      this.translateXMsgItem(this.lastShowMsgId, 0, 200);
      this.lastShowMsgId = "";
      return;
    }
    this.firstTouchX = e.touches[0].clientX;
    this.firstTouchY = e.touches[0].clientY;
    if (this.firstTouchX > this.swipeCheckX) {
      this.swipeCheckState = 1;
    }
    this.lastMoveTime = e.timeStamp;
  },
  ontouchmove: function (e) {
    if (this.swipeCheckState === 0) {
      return;
    }
    //当开始触摸时有菜单显示时，不处理滑动操作
    if (this.touchStartState === 1) {
      return;
    }
    var moveX = e.touches[0].clientX - this.firstTouchX;
    var moveY = e.touches[0].clientY - this.firstTouchY;
    //已触发垂直滑动，由scroll-view处理滑动操作
    if (this.swipeDirection === 2) {
      return;
    }
    //未触发滑动方向
    if (this.swipeDirection === 0) {
      console.log(Math.abs(moveY));
      //触发垂直操作
      if (Math.abs(moveY) > 4) {
        this.swipeDirection = 2;
        this.setData({ scrollY: true });
        console.log(2222222222);
       // return;
      }
      //触发水平操作
      if (Math.abs(moveX) > 4) {
        console.log(11111111111);
        this.swipeDirection = 1;
        this.setData({ scrollY: false });
      }
      else {
        return;
      }

    }
    //禁用垂直滚动
    // if (this.data.scrollY) {
    //   this.setData({scrollY:false});
    // }

    this.lastMoveTime = e.timeStamp;
    //处理边界情况
    if (moveX > 0) {
      moveX = 0;
    }
    //检测最大左滑距离
    if (moveX < -this.maxMoveLeft) {
      moveX = -this.maxMoveLeft;
    }
    this.moveX = moveX;
    this.translateXMsgItem(e.currentTarget.dataset.index, moveX, 0);
  },
  ontouchend: function (e) {
    this.swipeCheckState = 0;
    var swipeDirection = this.swipeDirection;
    this.swipeDirection = 0;
    if (this.touchStartState === 1) {
      this.touchStartState = 0;
      this.setData({ scrollY: true });
      return;
    }
    //垂直滚动，忽略
    if (swipeDirection !== 1) {
      return;
    }
    if (this.moveX === 0) {
      this.showState = 0;
      //不显示菜单状态下,激活垂直滚动
      this.setData({ scrollY: true });
      return;
    }
    if (this.moveX === this.correctMoveLeft) {
      this.showState = 1;
      this.lastShowMsgId = e.currentTarget.id;
      return;
    }
    if (this.moveX < -this.thresholdMoveLeft) {
      this.moveX = -this.correctMoveLeft;
      this.showState = 1;
      this.lastShowMsgId = e.currentTarget.id;
    }
    else {
      this.moveX = 0;
      this.showState = 0;
      //不显示菜单,激活垂直滚动
      this.setData({ scrollY: true });
    }
    this.translateXMsgItem(e.currentTarget.dataset.index, this.moveX, 500);
    //this.translateXMsgItem(e.currentTarget.id, 0, 0);
  },
  getItemIndex: function (id) {
    var msgList = this.data.msgList;
    for (var i = 0; i < msgList.length; i++) {
      // if (msgList[i].id === id) {
      //   return i;
      // }
      if (i == id) {
        return i;
      }
    }
    return -1;
  },
  translateXMsgItem: function (id, x, duration) {
    var animation = wx.createAnimation({ duration: duration });
    animation.translateX(x).step();
    this.animationMsgItem(id, animation);
  },
  animationMsgItem: function (id, animation) {
    var index = this.getItemIndex(id);
    var param = {};
    var indexString = 'msgList[' + index + '].animation';
    param[indexString] = animation.export();
    this.setData(param);
  },
  animationMsgWrapItem: function (id, animation) {
    var index = this.getItemIndex(id);
    var param = {};
    var indexString = 'msgList[' + index + '].wrapAnimation';
    param[indexString] = animation.export();
    this.setData(param);
  },
})
