const app = getApp()
var http = require('../../../../service/http.js');
var getTime = require('../../../../utils/util.js');
var wechat = require('../../../../utils/wechat.js');
// pages/bdataEntry/bdataEntry.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    is_certificate:false,
    select: false,
    is_add: 1,
    tihuoWay: '',
    step: 1,
    p_id:'',
    disabled:false,
// 1
      policy_num:'',
      report_num:'',
      insurance_type:'',
// 2
      goods_type: '',
      goods_name: '',
      good_pg_price:'',
      goods_num: '',
      good_price: '',
// 3
      damage_at: '',
      damage_result: '',
      address: '',
      omit: '',
      city: '',
      classify: '',
      address_detail:'',
      other_report:[],
// 4
      present_at:'',
      goods_list:[],
      goods_list_srv:[],
      goods_desc: '',
      present_img: [],
    present_video:[],
      // present_video: [],
// 5
      should_at: '',
      rem_company: '',
      linkman: '',
      connect_phone: '',
// 6
      author_report: [],

    newTime: '',
    insuranceTypeList: [],
    goodsTypeList: [],
    goodsTypeIndex: '',
    insuranceTypeIndex: '',
    damageResultNum: 0,
    damageResultMaxNum: 200,
    goodsDescNum: 0,
    goodsDescMaxNum: 200,
    insuranceTypeSelected: '',
    insuranceTypeSelectedID: '',
    landed: false,
    showModal: false,
    showWebView: false,

    /* 一片天空   解决全屏播放视频 层级问题*/
    vodio_play:false,
    p_id:''
   
  },
  // **************
  onUnload () {
    // let _this = this
    // wx.showModal({
    //   title: '提示',
    //   content: '取消后是否要保存本次编辑？',
    //   success(res) {
    //     if (res.confirm) {
    //       _this.nextStep()
    //     }
    //   }
    // })
  },
  delAuthorReportFile(e){
    /*var author_report = this.data.author_report;
    var arr = [];
    for (var i = 0; i < author_report.length; i++) {
      console.log(e.currentTarget.dataset.index)
      if (i == e.currentTarget.dataset.index) {
      } else {
        arr.push(author_report[i])
      }
    }*/
    // imgList =
    this.setData({
      author_report: ''
    })
  },
  delGoodsFile(e){
    var goods_list = this.data.goods_list;
    var arr = [];
    for (var i = 0; i < goods_list.length; i++) {
      console.log(e.currentTarget.dataset.index)
      if (i == e.currentTarget.dataset.index) {
      } else {
        arr.push(goods_list[i])
      }
    }
    // imgList =
    this.setData({
      goods_list: arr
    })
  },
  delSanfile(e){
    console.log(e)
    var other_report = this.data.other_report;
    var arr = [];
    for (var i = 0; i < other_report.length; i++) {
      console.log(e.currentTarget.dataset.index)
      if (i == e.currentTarget.dataset.index) {
      } else {
        arr.push(other_report[i])
      }
    }
    // imgList =
    this.setData({
      other_report: arr
    })
  },
  delImg(e) {
    console.log(e)
    var present_img = this.data.present_img;
    var arr=[];
    for (var i = 0; i < present_img.length; i++) {
      console.log(e.currentTarget.dataset.index)
      if (i == e.currentTarget.dataset.index){
      }else{
        arr.push(present_img[i])
      }
    }
    // imgList =
    this.setData({
      present_img: arr
    })
  },
  delVideo(e) {
    console.log(e)
    var present_video = this.data.present_video;
    var arr = [];
    for (var i = 0; i < present_video.length; i++) {
      console.log(e.currentTarget.dataset.index)
      if (i == e.currentTarget.dataset.index) {
      } else {
        arr.push(present_video[i])
      }
    }
    this.setData({
      present_video: arr
    })
  },
  lastSubmit () {
    let author_report = this.data.author_report ? this.data.author_report:'';
      http.postReq('/index/Insurer/confirmSubmit', {
        policy_num: this.data.policy_num,
        author_report: author_report
      }, res => {
        // 提交
        wx.showToast({
          title: '提交成功',
          duration: 1500
        })
        setTimeout(() => {
          wx.navigateTo({
            url: '/pages/index/index',
          })
        }, 1500)
      })
  },

  /* 一片天空改动  视频播放  start 2018:12:23 */
  screenChange :function(event){
      this.setData({vodio_play:event.detail.fullScreen})
  },
   /* 一片天空改动  视频播放  end 2018:12:23 */
  nextStep(e) {
    console.log(this.data.step)

    if (e.target.dataset.index == '6') {
        if(this.data.author_report.length==0){
             wx.showToast({title: '请先上传授权书',icon: 'none'}) ;return;
        }
        let _this = this;
        wx.showModal({
            title: '提示',
            content: '资料先递交系统进行审核，审核通过后系统将递交给物权人确认，确认后消息提醒你',
            success(res) {
                if (res.confirm) {
                  _this.lastSubmit()
                }
            }
        })
      return false
    }
    

    /* 一片天空改动  添加验证  start 2018:12:23 */
    // if (this.data.step == 1) {
    //     if(!this.data.policy_num){
    //         wx.showToast({title: '报案号不能为空',icon: 'none'}) ;return;
    //     }
    //     if(!this.data.report_num){
    //         wx.showToast({title: '保单号号不能为空',icon: 'none'}) ;return;
    //     }
    //     if(!/^[0-9a-zA-Z]+$/.test(this.data.policy_num)){
    //         wx.showToast({title: '报案号只能含有数字和字母',icon: 'none'}) ;return;
    //     };
    //     if(!/^[0-9a-zA-Z]+$/.test(this.data.report_num)){
    //         wx.showToast({title: '保单号只能含有数字和字母',icon: 'none'}) ;return;
    //     };
    //     if(!this.data.insuranceTypeSelectedID){
    //         wx.showToast({title: '请选择出险险种',icon: 'none'}) ;return;
    //     }
    // }

    // if (this.data.step == 5) {
    //     if (!this.data.linkman || !this.data.connect_phone) {
    //         wx.showToast({title: '手机号与联系人不能为空',icon: 'none'}) ;return;
    //     }
    //   if (/[0-9]/g.test(this.data.linkman)) {
    //     wx.showToast({ title: '联系人不能包含数字', icon: 'none' }); return;
    //   }
    //     if(this.data.linkman.length>12){
    //         wx.showToast({title: '联系人不能超出12个字符',icon: 'none'}) ;return;
    //     }
    //     if (!/^1[3|4|7|5|8]\d{9}$/.test(this.data.connect_phone)) {
    //         wx.showToast({title: '手机号格式不正确',icon: 'none'}) ;return;
    //     } 
    // }
    /* 一片天空改动  添加验证  ende 2018:12:23 */
    console.log(this.data.goods_list)

    var is_add = this.data.is_add;
    let param = {
      step: this.data.step,
      // 1
      report_num: this.data.report_num,
      insurance_type: this.data.insuranceTypeSelectedID,
      policy_num: this.data.policy_num,
      is_add: (!!e.target.dataset.is_add && is_add == 1) ? 1 : 2,
      // 2
      goods_type: this.data.goods_type,
      goods_name: this.data.goods_name,
      goods_num: this.data.goods_num,
      good_price: this.data.good_price,
      good_pg_price: this.data.good_pg_price,

      // 3
      damage_at: this.data.damage_at,
      damage_result: this.data.damage_result,
      // address: this.data.address ? this.data.address.join(','):'',
      omit: this.data.address && this.data.address.length > 0 ? this.data.address[0] : '',
      city: this.data.address && this.data.address.length > 0 ? this.data.address[1] : '',
      classify: this.data.address && this.data.address.length > 0 ? this.data.address[2] : '',
      address_detail: this.data.address_detail,
      other_report: JSON.stringify(this.data.other_report),
      // 4
      present_at: this.data.present_at,
      goods_list: JSON.stringify(this.data.goods_list),      
      goods_desc: this.data.goods_desc,
      present_img: this.data.present_img && this.data.present_img.length ? JSON.stringify(this.data.present_img) : JSON.stringify([]),


      /* 一片天空改动  视频上传   present_video  将json字符串改为以数组形式上传   start 2018:12:23 */
      present_video: this.data.present_video && this.data.present_video.length ? JSON.stringify(this.data.present_video) : JSON.stringify([]),
      /* 一片天空改动  添加验证  end 2018:12:23 */

      // 5
      should_at: this.data.should_at,
      rem_company: this.data.rem_company,
      linkman: this.data.linkman,
      connect_phone: this.data.connect_phone,
      // 6
      author_report: this.data.author_report && this.data.author_report.length ? this.data.author_report[0] : '',
      p_id: this.data.p_id
    };

    console.log(param)

    http.postReq('/index/NewNeed/saveInsurer', param, res => {
      console.log(res)
      if (this.data.step == 1) {
        this.setData({
          step: JSON.parse(this.data.step) + 1,
          p_id: res.data.p_id
        })
      } else if (this.data.step == 5) {
        this.setData({
          step: JSON.parse(this.data.step)
        })
      } else {
        this.setData({
          step: JSON.parse(this.data.step) + 1
        })
      }
    })

    if (e.target.dataset.index == '5') {
      var step = JSON.parse(this.data.step) + 1
      wx.navigateTo({
        url: '/pages/index/bbxr/checkData/writeData/writeData?p_id=' + this.data.p_id + '&step=' + step,
      })
    }

  },
  preview:function(e){
    var url = e.currentTarget.dataset.src
    console.log(url)
    wx.previewImage({
      current: url, // 当前显示图片的http链接
      urls: [url] // 需要预览的图片http链接列表
    })
  },
  one_good_pg_price(e){
    this.setData({
      good_pg_price: e.detail.value
    })
  },
  one_policy_num(e) {
    this.setData({
      policy_num: e.detail.value
    })
  },
  one_report_num(e) {
    this.setData({
      report_num: e.detail.value
    })
  },
  two_goods_name(e) {
    this.setData({
      goods_name: e.detail.value
    })
  },
  two_goods_num(e) {
    this.setData({
      goods_num: e.detail.value
    })
  },
  two_good_price(e) {
    this.setData({
      good_price: e.detail.value
    })
  },
  //受损原因
  three_damage_result(e) {
    
    this.setData({
      damageResultNum: e.detail.value.length,
      damage_result: e.detail.value
    })
  },
  three_address_detail(e){
    
    this.setData({
      address_detail: e.detail.value
    })
  },
  four_goods_desc(e) {
    
    this.setData({
      goodsDescNum: e.detail.value.length,
      goods_desc: e.detail.value
    })
  },
  five_rem_company(e) {
   
    this.setData({
      rem_company: e.detail.value
    })
  },
  five_linkman(e) {
    
    this.setData({
      linkman: e.detail.value
    })
  },
  five_connect_phone(e) {
   
    this.setData({
      connect_phone: e.detail.value
    })
  },
  lookTemplate(){
    wx.navigateTo({
      url: '../template/template?step=' + this.data.step
    })
  },
  //选择时间
  bindShouldChange(e) {
   
    this.setData({
      should_at: e.detail.value
    })
  },
  bindPresentChange(e) {
 
    this.setData({
      present_at: e.detail.value
    })
  },
  bindDamageChange(e) {
  
    this.setData({
      damage_at: e.detail.value
    })
  },
  //选择省市区
  bindAddressChange(e) {
   
    // let adds = e.detail.value
    this.setData({
      address: e.detail.value
    })
  },
  toShowModal() {
    this.setData({showModal: true});
  },
  chooseModal(e) {
    console.log('chooseModal', e);
    var id = e.currentTarget.dataset.mitem.it_id;
    var list = this.data.insuranceTypeList;
    list.forEach(i => {
      if (i.it_id === id) {
        i.checked = !i.checked;
      }
    });
    this.setData({insuranceTypeList: list});
  },
  cancelModal() {
    this.setData({ showModal: false});
    // this.data.insuranceTypeList.forEach(i => {
    //   i.checked = false
    // });
    // this.setData({ showModal: false, insuranceTypeList: this.data.insuranceTypeList, insuranceTypeSelected:[],});
  },
  submitModal() {
    var insuranceTypeSelected = [];
    var insuranceTypeSelectedID = [];
    this.data.insuranceTypeList.forEach(i => {
      if (i.checked){
        insuranceTypeSelected.push(i.it_name);
        insuranceTypeSelectedID.push(i.it_id);
      }
    });
    this.setData({showModal: false, insuranceTypeSelected: insuranceTypeSelected.join(','), insuranceTypeSelectedID: insuranceTypeSelectedID.join(',')});
  },
  //货物类别
  getGoodsType(goodtype) {
    http.getReq('/index/index/goodsType',{}, res => {
      if (goodtype) {
        res.data.type_list.forEach((i,key)=> {
          if (i.gt_id == goodtype) {
            this.setData({
              goods_type: i.gt_id,
              goodsTypeIndex: key
            })
          }
        });
      }
      this.setData({
        goodsTypeList: res.data.type_list
      })
    })
  },
  //保险种类
  getInsuranceType(chosedid) {
    chosedid = chosedid ? chosedid :'';
    var chose_arr= chosedid.split(",");
    http.getReq('/index/index/getInsuranceType',{}, res => {
      var insuranceTypeSelected = [];
      var insuranceTypeSelectedID = [];
      res.data.type_list.forEach(i =>{
        i.checked = false
        if(chose_arr.indexOf(i.it_id) >-1){
            i.checked = true;
            insuranceTypeSelected.push(i.it_name);
            insuranceTypeSelectedID.push(i.it_id);
        }
      });
      this.setData({
        insuranceTypeSelected: insuranceTypeSelected.join(','),
        insuranceTypeSelectedID: insuranceTypeSelectedID.join(','),
        insuranceTypeList: res.data.type_list,
      })
    })
  },
  goodsTypeList(e) {
    let typeList = this.data.goodsTypeList;
    this.setData({
      goodsTypeIndex: e.detail.value,
      goods_type: typeList[e.detail.value].gt_id
    })
  },
  insuranceTypeList(e) {
    let typeList = this.data.insuranceTypeList;
    this.setData({
      insuranceTypeIndex: e.detail.value,
      insurance_type: typeList[e.detail.value].it_id
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    app.tabBar();

    //step判断
    if (options.step){
      this.setData({
        step: options.step
      })
      http.getReq('/index/NewNeed/getInsurerFirst', {
        p_id: options.p_id,
        type: options.type
      }, res => {
        this.setData({
          company_name: res.data.company_name,
          mobile: res.data.mobile,
          policy_num: res.data.policy_num || '',
          report_num: res.data.report_num || '',
          insurance_type: res.data.insurance_type || '',
          ui_id: res.data.ui_id || '',
          end_at: res.data.end_at || '',
          username: res.data.username || '',
          step:options.step,
          p_id:options.p_id
          // newTime: getTime(new Date())
        })
      })
    };

    if (options.p_id){
      http.getReq('/index/NewNeed/getInsurerFirst', {
        p_id: options.p_id,
        type: options.type
      }, res => {
          let _data = res.data
          let _this = this;
          this.getInsuranceType(_data.goods_info.insurance_type)
          this.getGoodsType(_data.goods_info.goods_type)
          this.setData({
            // 2
            goods_type: _data.goods_info.goods_type ? _data.goods_info.goods_type : '',
            goods_name: _data.goods_info.goods_name ? _data.goods_info.goods_name : '',
            goods_num: _data.goods_info.goods_num ? _data.goods_info.goods_num : '',
            good_price: _data.goods_info.good_price ? _data.goods_info.good_price : '',
            good_pg_price: _data.goods_info.good_pg_price ? _data.goods_info.good_pg_price : '',
            insurance_type_name: _data.goods_info.insurance_type_name ? _data.goods_info.insurance_type_name : '',
            // 3
            damage_at: _data.goods_info.damage_at ? _data.goods_info.damage_at : '',
            damage_result: _data.goods_info.damage_result ? _data.goods_info.damage_result : '',
            address: [_data.goods_info.omit, _data.goods_info.city, _data.goods_info.classify],
            address_detail: _data.goods_info.address_detail ? _data.goods_info.address_detail : '',
            other_report:  _data.goods_info.other_report.length>0 ?  _data.goods_info.other_report: [],
            // 4
            present_at: _data.goods_info.present_at ? _data.goods_info.present_at : '',
            goods_list: _data.goods_info.goods_list,
            goods_desc: _data.goods_info.goods_desc ? _data.goods_info.goods_desc : '',
            present_img: _data.goods_info.present_img,

            /* 一片天空修改  接口返回数组格式  无需 分割  start*/
            present_video: _data.goods_info.present_video ? _data.goods_info.present_video : [],
            /* 一片天空修改  end*/

            // 5
            should_at: _data.goods_info.should_at ? _data.goods_info.should_at : '',
            rem_company: _data.goods_info.rem_company ? _data.goods_info.rem_company : '',
            linkman: _data.goods_info.linkman ? _data.goods_info.linkman : '',
            connect_phone: _data.goods_info.connect_phone ? _data.goods_info.connect_phone : '',
            // 6
          })
      })
    }
    this.getInsuranceType() //出险险种
    this.getGoodsType() //货物类别

    // /*  一片天空添加  解决 保险人确认代录人资料时，点击上传授权书并修改 上一步操作错乱 start*/
    // if(options.from && options.from =='certificate'){
    //     this.setData({is_certificate: true})
    // }
    // /*  一片天空添加  end */
    // if (options.info){
    //     let _data = JSON.parse(options.info);
    //     console.log(_data)
    //     this.getInsuranceType(_data.goods_info.insurance_type);
    //     this.getGoodsType(_data.goods_info.goods_type);
    //     // console.log(_data.goods_info.other_report);
    //     let _this = this;
    //     this.setData({
    //         policy_num: _data.goods_info.policy_num ? _data.goods_info.policy_num : '',
    //         report_num: _data.goods_info.report_num ? _data.goods_info.report_num : '',
    //         insurance_type: _data.goods_info.insurance_type ? _data.goods_info.insurance_type : '',
    //         is_add: 2,
    //         // 2
    //         goods_type: _data.goods_info.goods_type ? _data.goods_info.goods_type : '',
    //         goods_name: _data.goods_info.goods_name ? _data.goods_info.goods_name : '',
    //         goods_num: _data.goods_info.goods_num ? _data.goods_info.goods_num : '',
    //         good_price: _data.goods_info.good_price ? _data.goods_info.good_price : '',
    //         good_pg_price: _data.goods_info.good_pg_price ? _data.goods_info.good_pg_price : '',
    //         // 3
    //         damage_at: _data.goods_info.damage_at ? _data.goods_info.damage_at : '',
    //         damage_result: _data.goods_info.damage_result ? _data.goods_info.damage_result : '',
    //         address: [_data.goods_info.omit, _data.goods_info.city, _data.goods_info.classify],
    //         // address: ['广东省', '广州市', '海珠区'],
    //         address_detail: _data.goods_info.address_detail ? _data.goods_info.address_detail : '',
    //         other_report: _data.goods_info.other_report.length>0 ?  _data.goods_info.other_report: [],
    //         // 4
    //         present_at: _data.goods_info.present_at ? _data.goods_info.present_at : '',
    //         goods_list: _data.goods_info.goods_list,
    //         goods_desc: _data.goods_info.goods_desc ? _data.goods_info.goods_desc : '',
    //         present_img: _data.goods_info.present_img,
    //         present_video: _data.goods_info.present_video,
    //         // 5
    //         should_at: _data.goods_info.should_at ? _data.goods_info.should_at : '',
    //         rem_company: _data.goods_info.rem_company ? _data.goods_info.rem_company : '',
    //         linkman: _data.goods_info.linkman ? _data.goods_info.linkman : '',
    //         connect_phone: _data.goods_info.connect_phone ? _data.goods_info.connect_phone : '',
    //         // 6
    //         author_report: _data.goods_info.author_report ? _data.goods_info.author_report : '',
    //         disabled:true
    //     })
     
    //     return false;
    // }

  },
  onReady(){
    this.setData({
      newTime: getTime(new Date())
    })
  },
  bindShowMsg() {
    this.setData({
      select: !this.data.select
    })
  },
  mySelect(e) {
    var name = e.currentTarget.dataset.name
    this.setData({
      tihuoWay: name,
      select: false
    });
  },
  preStep(e) {

    /*  一片天空添加  解决 保险人确认代录人资料时，点击上传授权书并修改 上一步操作错乱 */
    if(this.data.is_certificate){
        wx.navigateBack();return;
    }
    /*  一片天空添加 end*/
    this.setData({
      disabled:true,
      step: JSON.parse(e.target.dataset.index) - 1
    })
    // wx.showModal({
    //   title: '提示',
    //   content: '取消后是否要保存本次编辑',
    //   success: function (res) {
    //     if (res.confirm) {
    //       // console.log('用户点击确定')
    //       _this.setData({
    //         step: JSON.parse(e.target.dataset.index) - 1
    //       })
    //       _this.nextStep()
    //     }else{
    //       step: JSON.parse(e.target.dataset.index) - 1
    //     }
    //   }
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (!this.data.landed) {
      this.setData({landed: true});
      return;
    }
    if(!this.data.showWebView) {
      this.setData({showWebView: false});
      return;
    }
    http.getReq('/index/user/getRole',{},_res=>{
      http.postReq('/index/upload/get_upload_url', {
        phone: _res.data.phone
      }, res => {
        if (res.data.url) {
          var inList = false;
          var url = res.data.url;
          this.data.goods_list.forEach(i => {
            if (i.url === url) {
              inList = true;
            }
          });
          if (!inList) {
            var name = res.data.url_name;
            var list = this.data.goods_list;
            list.push({url: url, url_name: name});
            this.setData({goods_list: list});
          }
        }
    });
  });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  // joinPicture: function (e) {
  //   var index = e.currentTarget.dataset.index;
  //   var evalList = this.data.evalList;
  //   var that = this;
  //   var imgNumber = evalList[index].tempFilePaths;
  //   if (imgNumber.length >= 3) {
  //     wx.showModal({
  //       title: '',
  //       content: '最多上传三张图片',
  //       showCancel: false,
  //     })
  //     return;
  //   }
  //   wx.showActionSheet({
  //     itemList: ["从相册中选择", "拍照"],
  //     itemColor: "#f7982a",
  //     success: function (res) {
  //       if (!res.cancel) {
  //         if (res.tapIndex == 0) {
  //           that.chooseWxImage("album", imgNumber);
  //         } else if (res.tapIndex == 1) {
  //           that.chooseWxImage("camera", imgNumber);
  //         }
  //       }
  //     }
  //   })
  // }, //选择视频

  // chooseVideo: function () {

  //   var that = this

  //   wx.chooseVideo({

  //     success: function (res) {

  //       that.setData({

  //         src: res.tempFilePath,

  //       })

  //     }

  //   })

  // },
  authorReportFile(){
    wechat.chooseImage()
      .then(d => {
        let { path, size } = d.tempFiles[0];
        wx.showLoading({ title: '上传中', mask: true });
        // let params = this.data.present_img;
        // params.push(path)
        // this.setData({
        //   tempImagePath: path,
        // });
        // console.log(path)
        console.log(size)
        if (size <= 10 * 1024 * 1024) {
          return wechat.uploadFile("/index/Upload/index", path, "avatar")
        } else {
          wx.showToast({
            title: '上传文件大小超过10M',
            icon: 'none',
            duration: 2000
          });
        }
      })
      .then(d => {
        wx.hideLoading();
        let res = JSON.parse(d.data);
        /*let author_report = this.data.author_report
        author_report[0] = res.data.url
        // author_report.push(res.data.url)
        console.log(author_report)*/
        this.setData({
          author_report: res.data.url
        })
      })
      .catch(e => {
        wx.hideLoading();
      })
  },
  // 第三方报告
  sanfile() {
    wechat.chooseImage()
      .then(d => {
        let { path, size } = d.tempFiles[0];
        wx.showLoading({title: '上传中', mask: true});
        // let params = this.data.present_img;
        // params.push(path)
        // this.setData({
        //   tempImagePath: path,
        // });
        // console.log(path)
        if (size <= 10 * 1024 * 1024) {
          return wechat.uploadFile("/index/Upload/index", path, "avatar")
        } else {
          wx.showToast({
            title: '上传文件大小超过10M',
            icon: 'none',
            duration: 2000
          });
        }
        
      })
      .then(d => {
        wx.hideLoading();
        let res = JSON.parse(d.data)
        let other_report = this.data.other_report
        other_report.push(res.data.url)
        console.log(other_report)
        this.setData({
          other_report: other_report
        })
      })
      .catch(e => {
        wx.hideLoading();
      })
  },
  // 货品清单
  goodsFile() {
    this.setData({showWebView: true});
    http.getReq('/index/user/getRole',{}, res=>{
      wx.navigateTo({
        url: '/pages/public/webView/webView?phone='+res.data.phone
      });
    })
  },
  camera(e) {
    let { type } = e.target.dataset;
    this.setData({type})
    console.log("开始上传准备", type == "chooseImage" ? "图片" : "视频");
    // 拍照
    if (type == "chooseImage") {
      wechat.chooseImage()
        .then(d => {
          wx.showLoading({title: '上传中', mask: true});
          let { path, size } = d.tempFiles[0];
          // let params = this.data.present_img;
          // params.push(path)
          // this.setData({
          //   tempImagePath: path,
          // });
          // console.log(path)
          //上传文件限制大小，当前10M
          
          if (size <= 10 * 1024 * 1024) {
            return wechat.uploadFile("/index/Upload/index", path, "avatar")
          } else {
            wx.showToast({
              title: '上传文件大小超过10M',
              icon: 'none',
              duration: 2000
            });
          }
        })
        .then(d => {
          wx.hideLoading();
          let res = JSON.parse(d.data)
          let present_img = this.data.present_img
          present_img.push(res.data.url)
          console.log(present_img)
          this.setData({
            present_img: present_img
          })
        })
        .catch(e => {
          wx.hideLoading();
        })
    }
    // 录视频
    else if (type == "chooseVideo") {

        /* 一片天空改动  上传视屏  satrt 2018:12:23 */
        var  _this = this;
        wx.chooseVideo({
            sourceType: ['album', 'camera'],
            maxDuration: 60,
            camera: 'back',
            success(res) {
                wx.showLoading({title: '上传中', mask: true});
                var size = res.size ; 
                var tempFilePath = res.tempFilePath;
                if (size > 10 * 1024 * 1024){ 
                    wx.showToast({title: '视频不可超过10M',icon: 'none',duration: 2000}); return;
                }
                wechat.uploadFile("/index/Upload/index", tempFilePath, "avatar").then(d => {
                    wx.hideLoading();
                    var re = JSON.parse(d.data);
                    if(re.code == 200){
                        var present_video = _this.data.present_video
                        present_video.push(re.data.url)
                        console.log(present_video)
                        _this.setData({present_video: present_video})
                    }else{
                        wx.showToast({title: '视频上传失败',icon: 'none',duration: 2000});
                    }
                });    
            },
            fail(res){
                // wx.showToast({title: '视频上传失败,文件大小不可超过10M',icon: 'none',duration: 2000});
            }
        })
        /* 一片天空改动  上传视屏  end 2018:12:23 */
     
    }
  }
  // chooseWxImage: function (type, list) {
  //   var img = list;
  //   var len = img.length;
  //   var that = this;
  //   var evalList = this.data.evalList;
  //   wx.chooseImage({
  //     count: 3,
  //     sizeType: ["original", "compressed"],
  //     sourceType: [type],
  //     success: function (res) {
  //       var addImg = res.tempFilePaths;
  //       var addLen = addImg.length;
  //       if ((len + addLen) > 3) {
  //         for (var i = 0; i < (addLen - len); i++) {
  //           var str = {};
  //           str.pic = addImg[i];
  //           img.push(str);
  //         }
  //       } else {
  //         for (var j = 0; j < addLen; j++) {
  //           var str = {};
  //           str.pic = addImg[j];
  //           img.push(str);
  //         }
  //       }
  //       that.setData({
  //         evalList: evalList
  //       })
  //       that.upLoadImg(img);
  //     },
  //   })
  // },
  // upLoadImg: function (list) {
  //   var that = this;
  //   this.upload(that, list);
  // },
  // //多张图片上传
  // upload: function (page, path) {
  //   var that = this;
  //   var curImgList = [];
  //   for (var i = 0; i < path.length; i++) {
  //       wx.uploadFile({
  //         url: app.globalData.subDomain + '/API/AppletApi.aspx',//接口处理在下面有写
  //         filePath: path[i].pic,
  //         name: 'file',
  //         header: { "Content-Type": "multipart/form-data" },
  //         formData: {
  //           douploadpic: '1'
  //         },
  //         success: function (res) {
  //           curImgList.push(res.data);
  //           var evalList = that.data.evalList;
  //           evalList[0].imgList = curImgList;
  //           that.setData({
  //             evalList: evalList
  //           })
  //           if (res.statusCode != 200) {
  //             wx.showModal({
  //               title: '提示',
  //               content: '上传失败',
  //               showCancel: false
  //             })
  //             return;
  //           }
  //           var data = res.data
  //           page.setData({  //上传成功修改显示头像
  //             src: path[0]
  //           })
  //         },
  //         fail: function (e) {
  //           wx.showModal({
  //             title: '提示',
  //             content: '上传失败',
  //             showCancel: false
  //           })
  //         },
  //         complete: function () {
  //           wx.hideToast();  //隐藏Toast
  //         }
  //       })
  //   }
  // },
})
