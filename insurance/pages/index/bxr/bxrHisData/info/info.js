// pages/index/bxr/bxrHisData/bxrInfo/bbxrInfo.js
var http = require('../../../../../service/http.js')
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    present_img: [],
    present_video: [],
    goodsInfo: {},
    policy_return:'',
    goods_return:	'',
    under_return:	'',
    situation_return:'',
    info_return:	'',
    certificate_return:'',
    editMsg: '',
    p_id:'',
    type:1,
    bolDaysList:[],
    role_id:'',
    showModal: false, //自定义弹窗
    modalShow:false,
    weituoMsg:''
  },
  download(e){
    http.getReq('/index/NewNeed/downloadAuth', {
      p_id: this.data.p_id
    }, res => {
      console.log(res)
      wx.setClipboardData({
        data: res.data.entrust_url,
        success: function (res) {
          wx.getClipboardData({
            success: function (res) {
              wx.showToast({
                title: '复制成功，请在浏览器打开下载',
                icon: 'none',
                duration: 3000
              })
            }
          })
        }
      })
    })
  },
  downloadFile (e) {
    let url = e.currentTarget.dataset.url
    wx.downloadFile({
      url: url,
      header: {},
      success: function (res) {
        var filePath = res.tempFilePath;
        wx.openDocument({
          filePath: filePath,
          success: function (res) {
            console.log('打开文档成功')
          },
          fail: function (res) {
            wx.showToast({
              title: 'openDocument:fail filetype not supported',
              icon: 'none'
            });
            console.log(res);
          },
          complete: function (res) {
            console.log(res);
          }
        })
      },
      fail: function (res) {
        wx.showToast({
          title: '文件下载失败',
          icon: 'none'
        });
        console.log('文件下载失败');
      },
      complete: function (res) { },
    })
  },
  tab(e){
    var type = e.currentTarget.dataset.type
    this.setData({
      type:type
    })
    this.getInfo()
  },
  bohui(){
    this.setData({
      showModal:true
    })
  },
  noPass(){
    this.setData({
      modalShow:true
    })
  },
  bottonBtn2(){
    var ads = this.data.address,
        newsAds = ads.split(',');
        console.log(newsAds)
    var province = newsAds[0],
        city = newsAds[1],
        town = newsAds[2];
    var parm = {
      policy_num: this.data.goodsInfo.goods_info.policy_num,
      goods_type: this.data.goodsInfo.goods_info.goods_type,
      goods_name: this.data.goodsInfo.goods_info.goods_name,
      goods_num: this.data.goodsInfo.goods_info.goods_num,
      good_price: this.data.goodsInfo.goods_info.good_price,
      good_pg_price: this.data.goodsInfo.goods_info.good_pg_price,
      damage_at: this.data.goodsInfo.goods_info.damage_at,
      damage_result: this.data.goodsInfo.goods_info.damage_result,
      province: province,
      city: city,
      town: town,
      address: this.data.goodsInfo.goods_info.address,
      address_detail: this.data.goodsInfo.goods_info.address_detail,
      // other_report: this.data.goodsInfo.goods_info.other_report ? this.data.goodsInfo.goods_info.other_report.join(','):'',
      other_report: JSON.stringify(this.data.goodsInfo.goods_info.other_report),
      present_at: this.data.goodsInfo.goods_info.present_at,
      goods_list: JSON.stringify(this.data.goodsInfo.goods_info.goods_list),
      // goods_desc: this.data.goodsInfo.goods_info.goods_desc ? this.data.goodsInfo.goods_info.goods_desc.join(','):'',
      goods_desc: this.data.goodsInfo.goods_info.goods_desc,
      present_img: JSON.stringify(this.data.goodsInfo.goods_info.present_img),
      present_video: JSON.stringify(this.data.goodsInfo.goods_info.present_video),
      
      should_at: this.data.goodsInfo.goods_info.should_at,
      rem_company: this.data.goodsInfo.goods_info.rem_company,
      linkman: this.data.goodsInfo.goods_info.linkman,
      connect_phone: this.data.goodsInfo.goods_info.connect_phone,
      now_status:10,
      is_return:10
    }
    // http.getReq('/index/Insurer/insurerInfo', {
    http.getReq('/index/Insurer/update', parm, res => {
      wx.showToast({
        title: '修改成功',
      })
      setTimeout(() => {
        wx.navigateTo({
          url: '/pages/index/index',
        })
      }, 800);
    })
  },
  //修改原因
  // editTurn() {
  //   wx.showModal({
  //     title: '提示',
  //     content: '未提交驳回原因，不予提交',
  //   })
  // },
  //修改原因
  // editTurn() {
  //   wx.showModal({
  //     title: '提示',
  //     content: '未提交驳回原因，不予提交',
  //   })
  // },
  //提交驳回信息
  bohuiTap(){

    if(!this.data.policy_return && !this.data.goods_return && !this.data.under_return && !this.data.situation_return && !this.data.info_return && !this.data.certificate_return){
        wx.showModal({
            title: '提示',
            content: '未提交驳回原因，不予提交',
        })
        return;
    }
    http.getReq('/index/Insurer/reject', {
      p_id: this.data.p_id,
      policy_return: this.data.policy_return,
      goods_return: this.data.goods_return,
      under_return: this.data.under_return,
      situation_return: this.data.situation_return,
      info_return: this.data.info_return,
      certificate_return: this.data.certificate_return,
    }, res => {
        wx.showToast({
          title: res.msg,
        })
        setTimeout(function(){wx.navigateBack({delta: 1});},2000)
        // newTime: getTime(new Date())
    })
  },
  preview:function(e){
    var url = e.currentTarget.dataset.src
    console.log(url)
    wx.previewImage({
      current: url, // 当前显示图片的http链接
      urls: [url] // 需要预览的图片http链接列表
    })
  },
  hideModal: function () {
    this.setData({
      showModal: false
    });
  },
  weiHideModal: function () {
    this.setData({
      modalShow: false
    });
  },
  onCancel: function () {
    this.hideModal();
  },
  weiCancel: function () {
    this.weiHideModal();
  },
  pass:function(){
    http.getReq('/index/NewNeed/passInsurer', {
      p_id: this.data.p_id
    }, res => {
        wx.showToast({
          title: res.msg,
        })
        setTimeout(function(){wx.navigateBack({delta: 1});},2000)
    })
  },
  weiPass:function(){
    http.getReq('/index/NewNeed/passAuth', {
      p_id: this.data.p_id
    }, res => {
        wx.showToast({
          title: res.msg,
        })
        setTimeout(function(){wx.navigateBack({delta: 1});},2000)
    })
  },
  onConfirm: function () {
    this.hideModal();
    http.getReq('/index/NewNeed/notPassInsurer', {
      p_id: this.data.p_id,
      msg: this.data.editMsg
    }, res => {
        wx.showToast({
          title: res.msg,
        })
        setTimeout(function(){wx.navigateBack({delta: 1});},2000)
    })
  },
  weiConfirm: function () {
    this.weiHideModal();
    http.getReq('/index/NewNeed/notPassAuth', {
      p_id: this.data.p_id,
      msg: this.data.weituoMsg
    }, res => {
        wx.showToast({
          title: res.msg,
        })
        setTimeout(function(){wx.navigateBack({delta: 1});},2000)
    })
  },
  edit(e) {
    this.setData({
      showModal: true,
      editMsg: '',
      edittap: e.currentTarget.dataset.bohui
    })
  },
  getEditMsg(e) {
    this.setData({
      editMsg: e.detail.value
    })
  },
  getWeituoMsg(e) {
    this.setData({
      weituoMsg: e.detail.value
    })
  },
  editInfo(e){
    console.log(this.data.address)
    console.log(this.data.goodsInfo)
    var ads = this.data.address,
        newsAds = ads.split(',');
        console.log(newsAds)
    var omit = newsAds[0],
        city = newsAds[1],
        classify = newsAds[2],
        goodsInfo = this.data.goodsInfo;

    goodsInfo.goods_info.omit = omit
    goodsInfo.goods_info.city = city
    goodsInfo.goods_info.classify = classify
    console.log(goodsInfo)
    this.setData({goodsInfo:goodsInfo})
    
    setTimeout(function (){
      wx.navigateTo({
        url: '../goodsInfoEdit/goodsInfoEdit?step=' + e.currentTarget.dataset.step + '&info=' + JSON.stringify(goodsInfo),
      })
    },1000)
  },
  //上传授权书并修改
  updateCertificate(e) {
    wx.navigateTo({
      url: '../../bdataEntry/bdataEntry?info=' + JSON.stringify(e.currentTarget.dataset.info) + '&step=' + e.currentTarget.dataset.step +"&from=certificate",
    })
  },
  getInfo(){
    http.getReq('/index/NewNeed/getInsurerFirst',{
      p_id: this.data.p_id,
      type: this.data.type
    }, res => {
      var role_id = parseInt(wx.getStorageSync('role_id'))

      if (this.data.type==1) {
        if (typeof res.data.goods_info.present_video == 'string') {
          /* 一片天空 修改   start  数据格式 有 数组形式字符串  和纯字符串*/
          var str = res.data.goods_info.present_video;
          if(str.indexOf("[")>=0 && str.indexOf("]">=0)){
              res.data.goods_info.present_video = JSON.parse(str);
          }else{
              res.data.goods_info.present_video = res.data.goods_info.present_video.split(',')
          }
          /* 一片天空 修改   end*/
        }
        if (typeof res.data.goods_info.present_img == 'string') {
          res.data.goods_info.present_img = res.data.goods_info.present_img.split(',')
        }

        // res.data.new_status = Number(res.data.new_status)
        this.setData({
          goodsInfo: res.data,
          address: res.data.goods_info.omit + ',' + res.data.goods_info.city + ',' + res.data.goods_info.classify,
          role_id:role_id
          // newTime: getTime(new Date())
        })
        console.log(this.data.goodsInfo.new_status)
        console.log(this.data.role_id)
      };

      if (this.data.type == 2) {
        var goodsInfo = res.data
        goodsInfo.goods_info.address = res.data.goods_info.province + ',' + res.data.goods_info.city + ',' + res.data.goods_info.town
        goodsInfo.goods_info.bol_address = res.data.goods_info.bol_province + ',' + res.data.goods_info.bol_city + ',' + res.data.goods_info.bol_town
        goodsInfo.goods_info.bol_days = Number(goodsInfo.goods_info.bol_days)
        goodsInfo.new_status = Number(goodsInfo.new_status)

        this.setData({
          goodsInfo: res.data,
          role_id:role_id
        })
        console.log(this.data.goodsInfo)
        console.log(this.data.role_id)

      };
      
    })
  },
  editBtn(e){
    var step = e.currentTarget.dataset.step
    if (step>=1 && step<=5) {
      wx.navigateTo({
        url: '../../bdataEntry/bdataEntry?p_id=' + this.data.p_id + '&step=' + step + '&type=' + this.data.type,
      })
    };
    if (step>=6 && step<=10) {
      wx.navigateTo({
        url: '../../../bbxr/checkData/writeData/writeData?p_id=' + this.data.p_id + '&step=' + step + '&type=' + this.data.type,
      })
    };
    // wx.navigateTo({
    //   url: '../../bdataEntry/bdataEntry?info=' + JSON.stringify(e.currentTarget.dataset.info),
    // })
  },
  delBtn(e){
    var that = this;
    wx.showModal({
      title:'删除',
      content:'确认要删除该信息么？',
      success:function(res) {
        if (res.confirm) {
          http.getReq('/index/Insurer/insurerInfo',{
            p_id: that.data.p_id
          }, res => {
            wx.navigateTo({
              url: '/pages/index/bxr/bxrHisData/bxrHisData',
            })
          })
        }
      }
    })
  },
  getBolDays() {
    http.getReq('/index/insurant/getBolDays', {}, res => {
      this.setData({
        bolDaysList: res.data,
        // newTime: getTime(new Date())
      })
      console.log(this.data.bolDaysList)
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      p_id: options.p_id
    })
    this.getInfo()
    console.log(this.data.p_id)
    this.getBolDays()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
