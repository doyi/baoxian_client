const app = getApp()
// pages/bdataEntry/bdataEntry.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    select: false,
    tihuoWay: '',
    step: 1,
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options,data) {
    console.log(options,data)
    this.setData({
      step:options.step
    })
    app.tabBar();
  },
  bindShowMsg() {
    this.setData({
      select: !this.data.select
    })
  },
  mySelect(e) {
    var name = e.currentTarget.dataset.name
    this.setData({
      tihuoWay: name,
      select: false
    });
  },
  nextStep(e) {
    if (e.target.dataset.index == '6') {
      // 提交
      wx.navigateBack()
    }
    this.setData({
      step: JSON.parse(e.target.dataset.index) + 1
    })
  },
  preStep(e) {
    this.setData({
      step: JSON.parse(e.target.dataset.index) - 1
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})