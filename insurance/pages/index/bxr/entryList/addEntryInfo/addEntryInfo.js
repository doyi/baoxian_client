// pages/index/entryList/addEntryInfo/addEntryInfo.js
var http = require('../../../../../service/http.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    recordor_id: '',
    realName: '',
    mobile: '',
    card_num:'',
    rem_comname:'',
    rem_link_phone:'',
    rem_link_name:'',
    array:'',
    insuranceTypeIndex:''
  },
  getRem_link_phone(e){
    this.setData({
      rem_link_phone: e.detail.value
    })
  },
  getRem_comname(e){
    this.setData({
      rem_comname: e.detail.value
    })
  },
  getRem_link_name(e){
    this.setData({
      rem_link_name: e.detail.value
    })
  },
  getRealName(e) {
    this.setData({
      realName: e.detail.value
    })
  },
  getCard(e) {
    this.setData({
      card_num: e.detail.value
    })
  },
  getMobile(e) {
    this.setData({
      mobile: e.detail.value
    })
  },
  insuranceTypeList(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      insuranceTypeIndex: e.detail.value,
    })
  },
  saveRecorder() {
    if (!this.data.realName.trim()) {
      wx.showToast({title: '请填写姓名', icon: 'none', duration:2000});
      return;
    }
    if (!this.data.mobile.trim()) {
      wx.showToast({title: '请填写手机号', icon: 'none', duration:2000});
      return;
    }
    if (!this.data.card_num.trim()) {
      wx.showToast({title: '请填写身份证', icon: 'none', duration:2000});
      return;
    }
    if (this.data.insuranceTypeIndex === '') {
      wx.showToast({title: '请选择代录人类型', icon: 'none', duration:2000});
      return;
    }
    if (!this.data.rem_comname.trim()) {
      wx.showToast({title: '请填写单位名称', icon: 'none', duration:2000});
      return;
    }
    if (!this.data.rem_link_name.trim()) {
      wx.showToast({title: '请填写单位联系人', icon: 'none', duration:2000});
      return;
    }
    if (!this.data.rem_link_phone.trim()) {
      wx.showToast({title: '请填写单位联系人电话', icon: 'none', duration:2000});
      return;
    }
    var url = this.data.recordor_id ? '/index/Recordor/editRecorder' : '/index/Insurer/bindOther';
    var data = {
      realName: this.data.realName,
      card_num: this.data.card_num,
      rem_comname: this.data.rem_comname,
      rem_link_name: this.data.rem_link_name,
      rem_link_phone: this.data.rem_link_phone,
      recoder_type: this.data.insuranceTypeIndex?this.data.array[this.data.insuranceTypeIndex].id:'',
    };
    if (this.data.recordor_id) {
      data.recordor_id = this.data.recordor_id;
    } else {
      data.mobile = this.data.mobile;
    }
    http.getReq(url, data, res => {
      wx.showToast({
        title: '保存成功',
        icon: 'success',
      })
      setTimeout(() => {
        wx.navigateBack();
      }, 800);
    })
  },
  // 获取代录人列表
  getRecorderType(recorder_type) {
    http.getReq('/index/index/recorderType', {}, res => {
      let insuranceTypeIndex = '';
      res.data.type_list.forEach((v, index) => {
        if (v.id === recorder_type) {
          insuranceTypeIndex = index;
        }
      });
      this.setData({
        insuranceTypeIndex,
        array: res.data.type_list,
      });
    });
  },
  getUserInfo: function(id) {
    http.getReq('/index/Recordor/recordorInfo', {recordor_id: id}, res => {
      this.setData({
        recordor_id: id,
        realName: res.data.username || '',
        mobile: res.data.mobile || '',
        card_num: res.data.card_num || '',
        rem_comname: res.data.rem_comname || '',
        rem_link_phone: res.data.rem_link_phone || '',
        rem_link_name: res.data.rem_link_name || ''
      });
      this.getRecorderType(res.data.recorder_type);
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(JSON.parse(decodeURIComponent(options.recordor)))
    wx.setNavigationBarTitle({title: options.recordor ? '编辑代录人资料' : '添加代录人'});
    if (options.recordor) {
      var recordor = JSON.parse(decodeURIComponent(options.recordor));
      this.getUserInfo(recordor.recordor_id);
    } else {
      this.getRecorderType();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
