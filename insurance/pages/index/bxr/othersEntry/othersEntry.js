// pages/index/othersEntry/othersEntry.js
var http = require('../../../../service/http.js');
var getTime = require('../../../../utils/util.js');
Page({
  /**
   * 页面的初始数据
   */
  data: {
    newTime: '',
    array: [],
    report_num: '',
    policy_num: '',
    insurance_type:'', //险种
    end_at:'',
    recordor_id:'',
    insuranceTypeIndex:'',
    username:'',
    info:'',
  },
  submitModal() {
    var insuranceTypeSelected = [];
    var insuranceTypeSelectedID = [];
    this.data.array.forEach(i => {
      if (i.checked) {
        insuranceTypeSelected.push(i.it_name);
        insuranceTypeSelectedID.push(i.it_id);
      }
    });
    this.setData({ showModal: false, insuranceTypeSelected: insuranceTypeSelected.join(','), insuranceTypeSelectedID: insuranceTypeSelectedID.join(',') });
  },
  chooseModal(e) {
    console.log('chooseModal', e);
    var id = e.currentTarget.dataset.mitem.it_id;
    var list = this.data.array;
    list.forEach(i => {
      if (i.it_id === id) {
        i.checked = !i.checked;
      }
    });
    this.setData({ array: list });
  },
  //保险种类
  getInsuranceType() {
    http.getReq('/index/index/getInsuranceType', {}, res => {
      res.data.type_list.forEach(i => {
        i.checked = false
      });
      this.setData({
        array: res.data.type_list,
      })
    })
  },
  toShowModal() {
    this.setData({ showModal: true });
  },
  cancelModal() {
    this.setData({ showModal: false });
  },
  insuranceTypeList(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      insuranceTypeIndex: e.detail.value,
    })
  },
  otherInsurer(){
    if (!this.data.policy_num){
      wx.showToast({ title: '保单号不能为空', icon: 'none', duration: 2000});
      return;
    }
    if (!this.data.report_num) {
      wx.showToast({ title: '报案号不能为空', icon: 'none', duration: 2000 });
      return;
    }
    if (!this.data.insuranceTypeSelected) {
      wx.showToast({ title: '请选择出险险种', icon: 'none', duration: 2000 });
      return;
    }
    if (!this.data.info.recordor_id) {
      wx.showToast({ title: '请选代录人', icon: 'none', duration: 2000 });
      return;
    }
    if (!this.data.end_at) {
      wx.showToast({ title: '请选择录入递交截至时间', icon: 'none', duration: 2000 });
      return;
    }
    http.getReq('/index/Insurer/otherInsurer', {
      policy_num: this.data.policy_num,
      report_num: this.data.report_num,
      insurance_type: this.data.insuranceTypeSelectedID,
      end_at: this.data.end_at,
      recordor_id: this.data.info.recordor_id ? this.data.info.recordor_id : '',
    }, res => {
      wx.showToast({
        title: '提交成功',
        icon: 'none'
      })
      setTimeout(()=>{
        wx.navigateTo({
          url: '../../index',
        })
      }, 800)
    })
  },
  bindDateChange(e){
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      end_at: e.detail.value
    })
  },
  getPolicy_num(e) {
    this.setData({
      policy_num: e.detail.value
    })
  },
  getReport_num(e) {
    this.setData({
      report_num: e.detail.value
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  insuranceTypeList(e){
    console.log('picker发送选择改变，携带值为', e.detail.value)
    this.setData({
      insuranceTypeIndex: e.detail.value,
    })
  },
  selectEntry(){
    wx.navigateTo({
      url: '/pages/index/bxr/entryList/entryList?selectEntry=1',
      success: function(res) {},
      fail: function(res) {},
      complete: function(res) {},
    })
  },
  onLoad: function (options) {
    this.getInsuranceType();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */

  onReady() {
    // this.setData({
    //   newTime: getTime(new Date())
    // })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})
