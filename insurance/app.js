//app.js
var http = require('./service/http.js')
App({
  // onLaunch: function () {
  //   // 展示本地存储能力
  //   var logs = wx.getStorageSync('logs') || []
  //   logs.unshift(Date.now())
  //   wx.setStorageSync('logs', logs)

  //   // 登录
  //   wx.login({
  //     success: res => {
  //       // 发送 res.code 到后台换取 openId, sessionKey, unionId
  //     }
  //   })
  //   // 获取用户信息
  //   wx.getSetting({
  //     success: res => {
  //       if (res.authSetting['scope.userInfo']) {
  //         // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
  //         wx.getUserInfo({
  //           success: res => {
  //             // 可以将 res 发送给后台解码出 unionId
  //             this.globalData.userInfo = res.userInfo

  //             // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
  //             // 所以此处加入 callback 以防止这种情况
  //             if (this.userInfoReadyCallback) {
  //               this.userInfoReadyCallback(res)
  //             }
  //           }
  //         })
  //       }
  //     }
  //   })
  // },
  ajaxJson: function (url, paraMap, callback, failCallback) {
    var me = this;
    var tokenMap = {};
    var timestamp = new Date().getTime();
    tokenMap.serverKey = "2016";
    tokenMap.timestamp = timestamp;
    for (var key in paraMap) {
      tokenMap[key] = paraMap[key];
    }
    var token = this.getToken(tokenMap, "bcttcwls789");
    paraMap.sign = token;
    paraMap.serverKey = "2016";
    paraMap.timestamp = timestamp;
    wx.request({
      url: this.getDomain() + url,
      method: "GET",
      header: {
        'content-type': 'application/json;charset=utf-8',
        "appid": wx.getStorageSync('appId'),
        "sessionid": wx.getStorageSync('sessionid') || "",
        "Cookie": "JSESSIONID=" + wx.getStorageSync('sessionid') || ""
      },
      data: paraMap,
      success: function (res) {
        console.log("invoke " + url + " Interface===========")
        if (callback) callback(res);
      },
      fail: function (err) {
        console.log("真机测试失败" + err.errMsg)
        if (failCallback) failCallback();
      },
      complete: function (res) { }
    })
  },
  // 动态导航
  tabBar: function(){
    if (this.globalData.userInfo.name == 'bxr'){
      return this.bxrBar();
    } else if (this.globalData.userInfo.name == 'bbxr') {
      return this.bbxrBar();
    } else if (this.globalData.userInfo.name == 'dlr') {
      return this.dlrBar();
    } else if (this.globalData.userInfo.name == 'ywy') {
      return this.ywyBar();
    }
  },
  // 保险人
  bxrBar: function () {
    var _curPageArr = getCurrentPages();
    var _curPage = _curPageArr[_curPageArr.length - 1];
    var _pagePath = _curPage.__route__;
    if (_pagePath.indexOf('/') != 0) {
      _pagePath = '/' + _pagePath;
    }
    var tabBar = this.globalData.bxrBar;
    for (var i = 0; i < tabBar.list.length; i++) {
      tabBar.list[i].active = false;
      if (tabBar.list[i].pagePath == _pagePath) {
        tabBar.list[i].active = true;//根据页面地址设置当前页面状态  
      }
    }
    _curPage.setData({
      tabBar: tabBar
    });
  },
  //被保险人的底部
  bbxrBar: function () {
    var _curPageArr = getCurrentPages();
    var _curPage = _curPageArr[_curPageArr.length - 1];
    var _pagePath = _curPage.__route__;
    if (_pagePath.indexOf('/') != 0) {
      _pagePath = '/' + _pagePath;
    }
    var tabBar = this.globalData.bbxrBar;
    for (var i = 0; i < tabBar.list.length; i++) {
      tabBar.list[i].active = false;
      if (tabBar.list[i].pagePath == _pagePath) {
        tabBar.list[i].active = true;//根据页面地址设置当前页面状态  
      }
    }
    _curPage.setData({
      tabBar: tabBar
    });
  },
  //代录入的底部
  dlrBar: function () {
    var _curPageArr = getCurrentPages();
    var _curPage = _curPageArr[_curPageArr.length - 1];
    var _pagePath = _curPage.__route__;
    if (_pagePath.indexOf('/') != 0) {
      _pagePath = '/' + _pagePath;
    }
    var tabBar = this.globalData.dlrBar;
    for (var i = 0; i < tabBar.list.length; i++) {
      tabBar.list[i].active = false;
      if (tabBar.list[i].pagePath == _pagePath) {
        tabBar.list[i].active = true;//根据页面地址设置当前页面状态  
      }
    }
    _curPage.setData({
      tabBar: tabBar
    });
  },
  //业务员的底部
  ywyBar: function () {
    var _curPageArr = getCurrentPages();
    var _curPage = _curPageArr[_curPageArr.length - 1];
    var _pagePath = _curPage.__route__;
    if (_pagePath.indexOf('/') != 0) {
      _pagePath = '/' + _pagePath;
    }
    var tabBar = this.globalData.ywyBar;
    for (var i = 0; i < tabBar.list.length; i++) {
      tabBar.list[i].active = false;
      if (tabBar.list[i].pagePath == _pagePath) {
        tabBar.list[i].active = true;//根据页面地址设置当前页面状态  
      }
    }
    _curPage.setData({
      tabBar: tabBar
    });
  },
  // getUserInfo: function (cb) {
  //   var that = this
  //   if (this.globalData.userInfo) {
  //     typeof cb == "function" && cb(this.globalData.userInfo)
  //   } else {
  //     //调用登录接口
  //     wx.login({
  //       success: function () {
  //         wx.getUserInfo({
  //           success: function (res) {
  //             that.globalData.userInfo = res.userInfo
  //             typeof cb == "function" && cb(that.globalData.userInfo)
  //           }
  //         })
  //       }
  //     })
  //   }
  // },
  globalData: {
    userInfo: { name:'bbxr'},
    time: "",
    bxrBar: {
      messageNum: 0,
      "color": "#a5a5a5",
      "selectedColor": "#ed2c28",
      "backgroundColor": "#fff",
      "borderStyle": "#white",
      "list": [
        {
          "pagePath": "/pages/index/index",
          "text": "首页",
          "iconPath": "https://xiaochengxu.rongyip.com/static/icons/home2.png",
          "selectedIconPath": "https://xiaochengxu.rongyip.com/static/icons/home1.png",
          "clas": "menu-item",
          "selectedColor": "#ed2c28",
          active: true
        },
        {
          "pagePath": "/pages/index/bxr/bdataEntry/bdataEntry",
          "text": "录入",
          "iconPath": "https://xiaochengxu.rongyip.com/static/icons/luru2.png",
          "selectedIconPath": "https://xiaochengxu.rongyip.com/static/icons/luru1.png",
          "selectedColor": "#ed2c28",
          "clas": "menu-item",
          active: false
        },
        {
          "pagePath": "/pages/index/msg/msg",
          "text": "消息",
          "iconPath": "https://xiaochengxu.rongyip.com/static/icons/xiaoxi2.png",
          "selectedIconPath": "https://xiaochengxu.rongyip.com/static/icons/xiaoxi1.png",
          "selectedColor": "#ed2c28",
          "clas": "menu-item",
          "alert": false,
          active: false
        },
        {
          "pagePath": "/pages/my/my",
          "text": "我的",
          "iconPath": "https://xiaochengxu.rongyip.com/static/icons/my2.png",
          "selectedIconPath": "https://xiaochengxu.rongyip.com/static/icons/my1.png",
          "selectedColor": "#ed2c28",
          "clas": "menu-item",
          active: false
        }
      ],
      "position": "bottom"
    },
    bbxrBar: {
      messageNum: 0,
      "color": "#a5a5a5",
      "selectedColor": "#ed2c28",
      "backgroundColor": "#fff",
      "borderStyle": "#white",
      "list": [
        {
          "pagePath": "/pages/index/index",
          "text": "首页",
          "iconPath": "https://xiaochengxu.rongyip.com/static/icons/home2.png",
          "selectedIconPath": "https://xiaochengxu.rongyip.com/static/icons/home1.png",
          "clas": "menu-item",
          "selectedColor": "#ed2c28",
          active: true
        },
        {
          "pagePath": "/pages/index/bbxr/checkData/checkData",
          "text": "核对资料",
          "iconPath": "https://xiaochengxu.rongyip.com/static/icons/luru2.png",
          "selectedIconPath": "https://xiaochengxu.rongyip.com/static/icons/luru1.png",
          "selectedColor": "#ed2c28",
          "clas": "menu-item",
          active: false
        },
        {
          "pagePath": "/pages/index/msg/msg",
          "text": "消息",
          "iconPath": "https://xiaochengxu.rongyip.com/static/icons/xiaoxi2.png",
          "selectedIconPath": "https://xiaochengxu.rongyip.com/static/icons/xiaoxi1.png",
          "selectedColor": "#ed2c28",
          "clas": "menu-item",
          "alert": false,
          active: false
        },
        {
          "pagePath": "/pages/my/my",
          "text": "我的",
          "iconPath": "https://xiaochengxu.rongyip.com/static/icons/my2.png",
          "selectedIconPath": "https://xiaochengxu.rongyip.com/static/icons/my1.png",
          "selectedColor": "#ed2c28",
          "clas": "menu-item",
          active: false
        }
      ],
      "position": "bottom"
    },
    dlrBar: {
      messageNum: 0,
      "color": "#a5a5a5",
      "selectedColor": "#ed2c28",
      "backgroundColor": "#fff",
      "borderStyle": "#white",
      "list": [
        {
          "pagePath": "/pages/index/index",
          "text": "首页",
          "iconPath": "https://xiaochengxu.rongyip.com/static/icons/home2.png",
          "selectedIconPath": "https://xiaochengxu.rongyip.com/static/icons/home1.png",
          "clas": "menu-item",
          "selectedColor": "#ed2c28",
          active: true
        },
        {
          "pagePath": "/pages/index/dlr/insuranceEntryList/insuranceEntryList",
          "text": "代录资料",
          "iconPath": "https://xiaochengxu.rongyip.com/static/icons/luru2.png",
          "selectedIconPath": "https://xiaochengxu.rongyip.com/static/icons/luru1.png",
          "selectedColor": "#ed2c28",
          "clas": "menu-item",
          active: false
        },
        {
          "pagePath": "/pages/index/msg/msg",
          "text": "消息",
          "iconPath": "https://xiaochengxu.rongyip.com/static/icons/xiaoxi2.png",
          "selectedIconPath": "https://xiaochengxu.rongyip.com/static/icons/xiaoxi1.png",
          "selectedColor": "#ed2c28",
          "clas": "menu-item",
          "alert": false,
          active: false
        },
        {
          "pagePath": "/pages/my/my",
          "text": "我的",
          "iconPath": "https://xiaochengxu.rongyip.com/static/icons/my2.png",
          "selectedIconPath": "https://xiaochengxu.rongyip.com/static/icons/my1.png",
          "selectedColor": "#ed2c28",
          "clas": "menu-item",
          active: false
        }
      ],
      "position": "bottom"
    },
    ywyBar: {
      messageNum: 0,
      "color": "#a5a5a5",
      "selectedColor": "#ed2c28",
      "backgroundColor": "#fff",
      "borderStyle": "#white",
      "list": [
        {
          "pagePath": "/pages/index/index",
          "text": "首页",
          "iconPath": "https://xiaochengxu.rongyip.com/static/icons/home2.png",
          "selectedIconPath": "https://xiaochengxu.rongyip.com/static/icons/home1.png",
          "clas": "menu-item",
          "selectedColor": "#ed2c28",
          active: true
        },
        {
          "pagePath": "/pages/index/ywy/enternum/index",
          "text": "报案号录入",
          "iconPath": "https://xiaochengxu.rongyip.com/static/icons/luru2.png",
          "selectedIconPath": "https://xiaochengxu.rongyip.com/static/icons/luru1.png",
          "selectedColor": "#ed2c28",
          "clas": "menu-item",
          active: false
        },
        {
          "pagePath": "/pages/index/msg/msg",
          "text": "消息",
          "iconPath": "https://xiaochengxu.rongyip.com/static/icons/xiaoxi2.png",
          "selectedIconPath": "https://xiaochengxu.rongyip.com/static/icons/xiaoxi1.png",
          "selectedColor": "#ed2c28",
          "clas": "menu-item",
          "alert": false,
          active: false
        },
        {
          "pagePath": "/pages/my/my",
          "text": "我的",
          "iconPath": "https://xiaochengxu.rongyip.com/static/icons/my2.png",
          "selectedIconPath": "https://xiaochengxu.rongyip.com/static/icons/my1.png",
          "selectedColor": "#ed2c28",
          "clas": "menu-item",
          active: false
        }
      ],
      "position": "bottom"
    }
  },
  setMessageNum: function(num) {
    var key = this.globalData.userInfo.name + 'Bar';
    var _curPageArr = getCurrentPages();
    var _curPage = _curPageArr[_curPageArr.length - 1];
    var tabBar = this.globalData[key];
    tabBar.messageNum = num;
    tabBar.list[2].count = false;
  },
  getMessageNum: function (){
    var key = this.globalData.userInfo.name + 'Bar';
    http.getReq('/index/message/check_msg', { type: 1 }, res => {
      var _curPageArr = getCurrentPages();
      var _curPage = _curPageArr[_curPageArr.length - 1];
      var tabBar = this.globalData[key];
      if (res.data.count > 0){
        tabBar.list[2].count = true;
      }else{
        tabBar.list[2].count = false;
      }

      _curPage.setData({
        tabBar: tabBar
      });
    }, true)
  }
})